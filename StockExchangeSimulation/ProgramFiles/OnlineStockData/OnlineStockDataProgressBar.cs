﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml;

namespace StockExchangeSimulation
{
    public class OnlineStockDataProgressBar : INotifyPropertyChanged
    {
        DispatcherTimer dispatcherTimer;

        public DateTime startTime;
        public DateTime endTime;

        public double _value;
        public double Value
        {
            get { return _value; }
            set
            {
                this._value = value;
                RaisePropertyChanged("Value");
            }
        }

        public int _range;
        public int Range
        {
            get { return _range; }
            set
            {
                this._range = value;
                RaisePropertyChanged("Range");
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public OnlineStockDataProgressBar()
        {
            startTime = DateTime.Now;
            Value = 0;
            _range = 60;
            DispatcherTimerSetup();
        }

        /// <summary>
        /// Timer setup
        /// </summary>
        public void DispatcherTimerSetup()
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = TimeSpan.FromMilliseconds(100);
            dispatcherTimer.Start();
        }

        /// <summary>
        /// Timer tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void dispatcherTimer_Tick(object sender, object e)
        {
            Value = (DateTime.Now - startTime).TotalMilliseconds / (double)1000;
        }

        /// <summary>
        /// Start timer
        /// </summary>
        public void Start()
        {
            dispatcherTimer.Start();
            startTime = DateTime.Now;
        }

        /// <summary>
        /// Pause timer
        /// </summary>
        public void Pause()
        {
            dispatcherTimer.Stop();
            Value = 0;
        }

        #region PRopertyChangeed
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }
}
