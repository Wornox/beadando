﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Microsoft.Toolkit.Uwp.Helpers;


namespace StockExchangeSimulation
{
    public class OnlineStockData
    {
        public AlphaVantage alphaVantage = new AlphaVantage();

        /// <summary>
        /// Constructor
        /// </summary>
        public OnlineStockData()
        {
           Timer timer = new Timer(alphaVantage);
        }
    }

    public class Timer
    {
        DispatcherTimer dispatcherTimer;
        readonly int refreshtime = 60;
        AlphaVantage alphaVantage;

        /// <summary>
        /// Consttructor
        /// </summary>
        /// <param name="alphaVantage"></param>
        public Timer(AlphaVantage alphaVantage)
        {
            this.alphaVantage = alphaVantage;
            DispatcherTimerSetup();
        }

        /// <summary>
        /// Timer setup
        /// </summary>
        public void DispatcherTimerSetup()
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, refreshtime);
            dispatcherTimer.Start();

        }

        /// <summary>
        /// Timer tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void dispatcherTimer_Tick(object sender, object e)
        {
            App.OnlineStockDataProgressBar.startTime = DateTime.Now;
            alphaVantage.RefreshDataAsync();
        }

    }

    public class AlphaVantage
    {
        /// <summary>
        /// Data comapny representativ
        /// </summary>
        public class WebData : INotifyPropertyChanged
        {
            public string Name { get; set; }
            public string Symbol { get; set; }

            public string Type { get; set; }
            public string Region { get; set; }
            public string MarketOpen { get; set; }
            public string MarketClose { get; set; }
            public string TimeZone { get; set; }
            public string Currency { get; set; }

            public double Open { get; set; }
            public double High { get; set; }
            public double Low { get; set; }
            public double Close { get; set; }
            public int Volume { get; set; }
            public DateTime DateTime { get; set; }


            private int investedStocks;
            public int InvestedStocks
            {
                get { return investedStocks; }
                set
                {
                    this.investedStocks = value;
                    RaisePropertyChanged("InvestedStocks");
                }
            }

            private bool refresh;
            public bool Refresh
            {
                get { return refresh; }
                set
                {
                    this.refresh = value;
                    RaisePropertyChanged("Refresh");
                }
            }

            public WebData()
            {

            }


            public WebData(WebData other)
            {
                Name = other.Name;
                Symbol = other.Symbol;
                Type = other.Type;
                Region = other.Region;
                MarketOpen = other.MarketOpen;
                MarketClose = other.MarketClose;
                TimeZone = other.TimeZone;
                Currency = other.Currency;
                Open = other.Open;
                High = other.High;
                Low = other.Low;
                Close = other.Close;
                Volume = other.Volume;
                DateTime = other.DateTime;
                InvestedStocks = other.InvestedStocks;
                Refresh = other.Refresh;
            }

            public override string ToString()
            {
                return Name + " " + Symbol + " " + Region;
            }

            #region PropertyChangedEventHandler

            public event PropertyChangedEventHandler PropertyChanged;

            protected void RaisePropertyChanged(string name)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(name));
                }
            }

            #endregion

        }

        public ObservableCollection<ObservableCollection<WebData>> historylist { get; set; }

        public ObservableCollection<WebData> searchResultList = new ObservableCollection<WebData>();

        public WebData searchResult = new WebData();

        LocalObjectStorageHelper SaveLoadhelper = new LocalObjectStorageHelper();
        readonly string SaveLoadhelperKey = "historylist";

        /// <summary>
        /// Constructor
        /// </summary>
        public AlphaVantage()
        {
            historylist = new ObservableCollection<ObservableCollection<WebData>>();
            LoadAsync();

        }

        ///<summary> 
        ///This method called from investment maganer seach button
        ///</summary>
        async public void GetDataFromSearchResultAsync()
        {
            bool found = false;
            foreach(var element in historylist)
            {
                if (element.ElementAt(0).Symbol == searchResult.Symbol) found = true;
            }

            if (!found)
            {
                string url = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=" + searchResult.Symbol + "&interval=1min&apikey=" + App.Player.AlphaVantageAPIKey;

                ObservableCollection<WebData> newList = new ObservableCollection<WebData>(); // új list a cégnek
                await GetSpecificDataAsync(url, newList, searchResult);

                //ha APi limit kérés miatt üres a newlist-akkor ne csináljon vele semmit
                if (newList.Count != 0)
                {
                    //megnézem h volt e már befektetve
                    foreach (var element in App.Player.webStockDataInvestmentList)
                    {
                        if (element.Symbol == newList.ElementAt(0).Symbol)
                        {
                            newList.ElementAt(0).InvestedStocks = element.Stocks;
                        }
                    }

                    historylist.Add(newList);
                    newList.ElementAt(0).Refresh = false;

                    SaveAsync();

                }
            }

        }

        ///<summary>
        ///This is called form timer
        ///</summary>
        async public void RefreshDataAsync()
        {
            int index = 0;
            List<int> refreshindexes = new List<int>();
            foreach (var company in historylist) // végigmegyek az összes vállalatok
            {
                if (company.ElementAt(0).Refresh) // ha refresh
                {
                    refreshindexes.Add(index);
                }
                index++;
            }

            foreach(int refindex in refreshindexes)
            {
                var company = historylist.ElementAt(refindex);

                WebData templateSave = new WebData(company.ElementAt(0)); //lementem a régi első adatot a céges infók miatt
                string url = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=" + company.ElementAt(0).Symbol + "&interval=1min&apikey=" + App.Player.AlphaVantageAPIKey;

                try
                {
                    ObservableCollection<WebData> newList = new ObservableCollection<WebData>(); // új list a cégnek

                    await GetSpecificDataAsync(url, newList, templateSave);

                    if (newList.Count != 0)
                    {
                        newList.ElementAt(0).InvestedStocks = templateSave.InvestedStocks;  
                        App.WebStockDatas.alphaVantage.historylist.RemoveAt(refindex);
                        historylist.Insert(refindex, newList);
                    }
                } catch
                {
                    //hiba
                }

            }

            SaveAsync();

        }

        ///<summary> Gets data from website</summary>
        ///<param name="url">Erről kéri le.</param>
        ///<param name="list">A list amihez hozzá fogja fűzni az adatokat</param>
        ///<param name="template">Ennek a template adatnak a propety-eit használja a céges adatok kitöltéséhez</param>
        async public Task GetSpecificDataAsync(string url, ObservableCollection<WebData> list, WebData template)
        {

            try
            {
                HttpClient client;
                string response;
                Newtonsoft.Json.Linq.JObject jobj;
                Newtonsoft.Json.Linq.JToken jdata = null;

                client = new HttpClient();

                response = await client.GetStringAsync(url);

                jobj = Newtonsoft.Json.Linq.JObject.Parse(response);

                jdata = jobj["Time Series (1min)"];

                int index = 0;

                foreach (var datagroup in jdata)
                {
                    string s = datagroup.ToString();
                    System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(@"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}");
                    System.Text.RegularExpressions.Match m = r.Match(s);
                    DateTime dt = DateTime.Parse(m.Value, CultureInfo.InvariantCulture);


                    WebData newData = new WebData(template); // a céges adatokat bemásolom (copy, újat hozok létre)

                    foreach (var dataline in datagroup)
                    {
                        newData.Open = double.Parse(dataline["1. open"].ToString(), CultureInfo.InvariantCulture);
                        newData.High = double.Parse(dataline["2. high"].ToString(), CultureInfo.InvariantCulture);
                        newData.Low = double.Parse(dataline["3. low"].ToString(), CultureInfo.InvariantCulture);
                        newData.Close = double.Parse(dataline["4. close"].ToString(), CultureInfo.InvariantCulture);
                        newData.Volume = Int32.Parse(dataline["5. volume"].ToString(), CultureInfo.InvariantCulture);
                        newData.DateTime = dt;
                        newData.Refresh = true;
                    }
                    list.Add(newData);
                    index++;
                }
            }
            catch
            {
              //Hibaüzenetet dobni a felhasnálónak
            }

        }

        /// <summary>
        /// Get company search list
        /// </summary>
        /// <param name="keyword"></param>
        public async void SearchForCompanyAsync(string keyword)
        {
            searchResultList.Clear();

            string url = "https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=" + keyword + "&apikey=" + App.Player.AlphaVantageAPIKey;

            try
            {

                HttpClient client = new HttpClient();

                string response = await client.GetStringAsync(url);

                Newtonsoft.Json.Linq.JObject jobj = Newtonsoft.Json.Linq.JObject.Parse(response);

                var jdata = jobj["bestMatches"];

                foreach (var datagroup in jdata)
                {
                    WebData searchdata = new WebData();
                    searchdata.Symbol = datagroup["1. symbol"].ToString();
                    searchdata.Name = datagroup["2. name"].ToString();
                    searchdata.Type = datagroup["3. type"].ToString();
                    searchdata.Region = datagroup["4. region"].ToString();
                    searchdata.MarketOpen = datagroup["5. marketOpen"].ToString();
                    searchdata.MarketClose = datagroup["6. marketClose"].ToString();
                    searchdata.TimeZone = datagroup["7. timezone"].ToString();
                    searchdata.Currency = datagroup["8. currency"].ToString();
                    searchResultList.Add(searchdata);
                }
            } catch
            {
                //Hibaüzenetet dobni a felhasnálónak
            }
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {
            try
            {
                await SaveLoadhelper.SaveFileAsync(SaveLoadhelperKey, historylist);
            } catch
            {
                //ha éppen foglalt a file, akkor excaptiont dob
            }
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <returns></returns>
        public async Task LoadAsync()
        {
            if (await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKey))
            {
                ObservableCollection<ObservableCollection<AlphaVantage.WebData>> temp = await SaveLoadhelper.ReadFileAsync<ObservableCollection<ObservableCollection<WebData>>>(SaveLoadhelperKey);
                if (temp != null) historylist = temp;
            }
        }
    }

}

