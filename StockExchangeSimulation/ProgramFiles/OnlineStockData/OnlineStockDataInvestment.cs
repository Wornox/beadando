﻿using System;
using System.ComponentModel;
using Windows.ApplicationModel.Resources;

namespace StockExchangeSimulation
{

    public class OnlineStockDataInvestment : INotifyPropertyChanged
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OnlineStockDataInvestment()
        {

        }

        /// <summary>
        /// Converter constructor
        /// </summary>
        /// <param name="webdata"></param>
        public OnlineStockDataInvestment(AlphaVantage.WebData webdata)
        {
            _stocks = webdata.InvestedStocks;
            _startStocks = webdata.InvestedStocks;

            Name = webdata.Name;
            Symbol = webdata.Symbol;
            High = webdata.High;
            Low = webdata.Low;
            Open = webdata.Open;
            Close = webdata.Close;
            DateTime = webdata.DateTime;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="investment"></param>
        public OnlineStockDataInvestment(OnlineStockDataInvestment investment)
        {
            Name = investment.Name;
            Symbol = investment.Symbol;
            High = investment.High;
            Low = investment.Low;
            Open = investment.Open;
            Close = investment.Close;
            DateTime = investment.DateTime;
            Stocks = investment.Stocks;
            Change = investment.Change;
            StocksChange = investment.StocksChange;
        }

        /// <summary>
        /// Copy method
        /// </summary>
        /// <param name="investment"></param>
        public void CopyProperties(OnlineStockDataInvestment investment)
        {
            Name = investment.Name;
            Symbol = investment.Symbol;
            High = investment.High;
            Low = investment.Low;
            Open = investment.Open;
            Close = investment.Close;
            DateTime = investment.DateTime;
            Stocks = investment.Stocks;
            Change = investment.Change;
            StocksChange = investment.StocksChange;

        }

        public string Name { get; set; }
        public string Symbol { get; set; }

        public DateTime DateTime { get; set; }

        private int _startStocks;

        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }

        private int _stocks;
        public int Stocks
        {
            get { return _stocks; }
            set
            {
                this._stocks = value;
                RaisePropertyChanged("Stocks");
            }
        }

        private int _stocksChange;
        public int StocksChange
        {
            get { return _stocksChange; }
            set
            {
                this._stocksChange = value;
                RaisePropertyChanged("StocksChange");
            }
        }

        /// <summary>
        /// Increase invested stocks
        /// </summary>
        /// <param name="number"></param>
        public void IncreaseStocks(int number)
        {
            int newStocks = _stocks + number;
            double newChange = _change + (High + Low)/2 * number;
            newChange = Math.Round(newChange, 5);
            if (App.Player.PlayerAmountOnline > newChange || _startStocks > newStocks)
            {
                Stocks = newStocks;
                Change = newChange;
                StocksChange = _stocks - _startStocks;
            }
        }

        /// <summary>
        /// Decrease stocks
        /// </summary>
        /// <param name="number"></param>
        public void DecreaseStocks(int number)
        {
            int newStocks = _stocks - number;
            double newChange = _change - (High + Low) / 2 * number;
            newChange = Math.Round(newChange, 5);
            if (App.Player.PlayerAmountOnline > newChange && newStocks >= 0)
            {
                Stocks = newStocks;
                Change = newChange;
                if (Change < 1 && Change > -1) Change = 0;
                StocksChange = _stocks - _startStocks;
            }
        }

        private double _change;
        public double Change
        {
            get { return _change; }
            set
            {
                this._change = value;
                RaisePropertyChanged("Change");
            }

        }

        /// <summary>
        /// Investmenet manager price info
        /// </summary>
        /// <returns></returns>
        public string Info()
        {
            return
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageName")+ "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageSymbol") + "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageHigh") + "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageLow")  + "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageOpen")  + "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageClose")  + "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageDateTime");
        }

        /// <summary>
        /// Invesment manager comapny info
        /// </summary>
        /// <returns></returns>
        public string Info2()
        {
            return
                 Name + "\n" +
                 Symbol + "\n" +
                 High + "\n" +
                 Low + "\n" +
                 Open + "\n" +
                 Close + "\n" +
                 DateTime;
        }

        #region PropertyChangedEventHandler

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion
    }

}
