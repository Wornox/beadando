﻿using System;
using System.Linq;
using Windows.ApplicationModel.Resources;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace StockExchangeSimulation
{
    /// <summary>
    /// Online page
    /// </summary>
    public sealed partial class OnlineStockDataPage : Page
    {
        public int dataGridSelectedIndex;

        OnlineStockDataInvestment investmentData;

        /// <summary>
        /// Constructor
        /// </summary>
        public OnlineStockDataPage()
        {
            dataGridSelectedIndex = -1;

            this.InitializeComponent();

            DisplayCountComboBox.SelectedItem = App.WebStocksDisplayCount.webStocksDisplayCount.ElementAt(0);

            //AlphaVantage Main Table
            DataGridColumnWebStockDataAlphaVantageName.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageName));
            DataGridColumnWebStockDataAlphaVantageSymbol.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageSymbol));
            DataGridColumnWebStockDataAlphaVantageRegion.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageRegion));
            DataGridColumnWebStockDataAlphaVantageMarketOpen.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageMarketOpen));
            DataGridColumnWebStockDataAlphaVantageMarketClose.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageMarketClose));
            DataGridColumnWebStockDataAlphaVantageTimeZone.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageTimeZone));
            DataGridColumnWebStockDataAlphaVantageDateTime.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageDateTime));
            DataGridColumnWebStockDataAlphaVantageCurrency.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageCurrency));
            DataGridColumnWebStockDataAlphaVantageOpen.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageOpen));
            DataGridColumnWebStockDataAlphaVantageHigh.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageHigh));
            DataGridColumnWebStockDataAlphaVantageLow.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageLow));
            DataGridColumnWebStockDataAlphaVantageClose.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageClose));
            DataGridColumnWebStockDataAlphaVantageVolume.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageVolume));
            DataGridColumnWebStockDataAlphaVantageInvestedStocks.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageInvestedStocks));
            DataGridColumnWebStockDataAlphaVantageRefresh.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageRefresh));

            //AlphaVantage History Table

            DataGridColumnWebStockDataAlphaVantageDateTimeHistory.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageDateTime));
            DataGridColumnWebStockDataAlphaVantageOpenHistory.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageOpen));
            DataGridColumnWebStockDataAlphaVantageHighHistory.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageHigh));
            DataGridColumnWebStockDataAlphaVantageLowHistory.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageLow));
            DataGridColumnWebStockDataAlphaVantageCloseHistory.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageClose));

            OnlinePageCandle.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(OnlinePageCandle));
            OnlinePageLine.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(OnlinePageLine));

            DisplayCountComboBox.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DisplayCountComboBox));
            NewCompanySearchButton.Content = ResourceLoader.GetForViewIndependentUse().GetString(nameof(NewCompanySearchButton));
            RemoveCompanySearchButton.Content = ResourceLoader.GetForViewIndependentUse().GetString(nameof(RemoveCompanySearchButton));

            SearchDialog.Title = ResourceLoader.GetForViewIndependentUse().GetString(nameof(SearchDialog));

            SearchDialog.PrimaryButtonText = ResourceLoader.GetForViewIndependentUse().GetString("GeneralSelect");
            SearchDialog.SecondaryButtonText = ResourceLoader.GetForViewIndependentUse().GetString("GeneralSearch");
            SearchDialog.CloseButtonText = ResourceLoader.GetForViewIndependentUse().GetString("GeneralClose");

            InvestmentDialog.Title = ResourceLoader.GetForViewIndependentUse().GetString("VariableInvestmentManager");
            InvestmentDialog.PrimaryButtonText = ResourceLoader.GetForViewIndependentUse().GetString("GeneralSave");
            InvestmentDialog.CloseButtonText = ResourceLoader.GetForViewIndependentUse().GetString("GeneralClose");

        }

        /// <summary>
        /// Search for new comapny
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void NewCompanySearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool save = false;
                bool close = false;

                while (!save && !close)
                {
                    ContentDialogResult result = await SearchDialog.ShowAsync();

                    if (result == ContentDialogResult.Primary)
                    {
                        //Select
                        if (App.WebStockDatas.alphaVantage.searchResultList.Count != 0)
                        {
                            save = true;
                            App.WebStockDatas.alphaVantage.GetDataFromSearchResultAsync();
                        }
                    }
                    else if (result == ContentDialogResult.Secondary)
                    {
                        //Search
                        string keyword = SearchTextBox.Text;
                        App.WebStockDatas.alphaVantage.SearchForCompanyAsync(keyword);
                    }
                    else
                    {
                        //Close
                        close = true;
                    }

                }
            } catch
            {

            }

        }

        /// <summary>
        /// Fill up the search result list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            object obj = (sender as ListView).SelectedItem;

            App.WebStockDatas.alphaVantage.searchResult = obj as AlphaVantage.WebData;

            if(App.WebStockDatas.alphaVantage.searchResult == null)
            {
                SearchTopResultRun.Text = "";
            } else
            {
                SearchTopResultRun.Text = App.WebStockDatas.alphaVantage.searchResult.ToString();
            }
        }

        /// <summary>
        /// Select comapny in table grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadDataGrid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                var data = ((sender as Telerik.UI.Xaml.Controls.Grid.RadDataGrid).SelectedItem) as System.Collections.ObjectModel.ObservableCollection<AlphaVantage.WebData>;

                dataGridSelectedIndex = App.WebStockDatas.alphaVantage.historylist.IndexOf(data);

                RefreshDisplayData();

            } catch
            {
                //hiba
            }
            
        }

        /// <summary>
        /// Refresh display data
        /// </summary>
        private void RefreshDisplayData()
        {
            if(dataGridSelectedIndex >= 0)
            {
                System.Collections.ObjectModel.ObservableCollection<AlphaVantage.WebData> displayList = new System.Collections.ObjectModel.ObservableCollection<AlphaVantage.WebData>();

                int DisplayCount = 0;
                if(App.WebStockDatas.alphaVantage.historylist.ElementAt(dataGridSelectedIndex).Count < (int)DisplayCountComboBox.SelectedItem)
                {
                    DisplayCount = App.WebStockDatas.alphaVantage.historylist.ElementAt(dataGridSelectedIndex).Count;
                }
                else
                {
                    DisplayCount = (int)DisplayCountComboBox.SelectedItem;
                }

                for (int i = 0; i < DisplayCount; i++)
                {
                    displayList.Add(App.WebStockDatas.alphaVantage.historylist.ElementAt(dataGridSelectedIndex).ElementAt(i));
                }

                selectedHistoryDataGrid.ItemsSource = displayList;

                selectedHistoryChart.HorizontalAxis.LabelInterval = DisplayCount / 10;

                selectedHistoryChart.DataContext = displayList;

                selectedHistoryLineChart.DataContext = displayList;

            }
        }

        /// <summary>
        /// Remove comapny from table grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveCompanySearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                App.WebStockDatas.alphaVantage.historylist.RemoveAt(dataGridSelectedIndex);
                dataGridSelectedIndex = -1;
            }
            catch
            {

            }
        }

        /// <summary>
        /// Graph data mouse cursor view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChartTrackBallBehavior_TrackInfoUpdated(object sender, Telerik.UI.Xaml.Controls.Chart.TrackBallInfoEventArgs e)
        {
            AlphaVantage.WebData DataItem = (e.Context.ClosestDataPoint.DataPoint.DataItem as AlphaVantage.WebData);
            e.Context.ClosestDataPoint.DisplayHeader = DataItem.DateTime;
        }

        /// <summary>
        /// Combo box selection changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DisplayCountComboBox.SelectedItem = (sender as ComboBox).SelectedItem;
            RefreshDisplayData();
        }

        /// <summary>
        /// Investment manager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void AlphavantageTable_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            try
            {
                var data = ((sender as Telerik.UI.Xaml.Controls.Grid.RadDataGrid).SelectedItem) as System.Collections.ObjectModel.ObservableCollection<AlphaVantage.WebData>;
                dataGridSelectedIndex = App.WebStockDatas.alphaVantage.historylist.IndexOf(data);

                investmentData = new OnlineStockDataInvestment(data.ElementAt(0));

                Border border = new Border
                {
                    BorderBrush = new SolidColorBrush(Windows.UI.Colors.Gray),
                    BorderThickness = new Thickness() { Bottom = 2, Left = 2, Right = 2, Top = 2 }
                };

                StackPanel stackPanel = new StackPanel();


                //Info Box
                RichTextBlock info = new RichTextBlock();

                Run runInfo = new Run
                {
                    Text = investmentData.Info()
                };

                Paragraph paragraph = new Paragraph();
                paragraph.Inlines.Add(runInfo);

                info.Height = 200;
                info.Width = 600;
                info.Blocks.Add(paragraph);

                //Info Box2
                RichTextBlock info2 = new RichTextBlock();

                Run runInfo2 = new Run
                {
                    Text = investmentData.Info2()
                };

                Paragraph paragraph2 = new Paragraph();
                paragraph2.Inlines.Add(runInfo2);

                info2.Height = 200;
                info2.Width = 600;
                info2.Blocks.Add(paragraph2);

                //Info grid
                Grid InfoGrid = new Grid();
                ColumnDefinition col1Info = new ColumnDefinition();
                ColumnDefinition col2Info = new ColumnDefinition();
                ColumnDefinition col3Info = new ColumnDefinition();
                col1Info.Width = new GridLength(1, GridUnitType.Star);
                col2Info.Width = new GridLength(1, GridUnitType.Star);
                col3Info.Width = new GridLength(1, GridUnitType.Star);

                InfoGrid.ColumnDefinitions.Add(col1Info);
                InfoGrid.ColumnDefinitions.Add(col2Info);
                InfoGrid.ColumnDefinitions.Add(col3Info);

                InfoGrid.Children.Add(info);
                InfoGrid.Children.Add(info2);

                Grid.SetColumn(info, 0);
                Grid.SetColumn(info2, 1);
                Grid.SetColumnSpan(info2, 2);

                stackPanel.Children.Add(InfoGrid);


                //Invesmtment Change Grid
                TextBlock textBlockChangeString = new TextBlock
                {
                    Text = ResourceLoader.GetForViewIndependentUse().GetString("GeneralInvestedPrice") + ": "
                };

                TextBlock textBlockChange = new TextBlock();
                textBlockChange.SetBinding(TextBlock.TextProperty, new Binding() { Path = new PropertyPath("Change"), Source = investmentData, Mode = BindingMode.OneWay });

                Grid InvestmentChangeGrid = new Grid();
                ColumnDefinition col1I = new ColumnDefinition();
                ColumnDefinition col2I = new ColumnDefinition();
                ColumnDefinition col3I = new ColumnDefinition();
                col1I.Width = new GridLength(1, GridUnitType.Star);
                col2I.Width = new GridLength(1, GridUnitType.Star);
                col3I.Width = new GridLength(1, GridUnitType.Star);

                InvestmentChangeGrid.ColumnDefinitions.Add(col1I);
                InvestmentChangeGrid.ColumnDefinitions.Add(col2I);
                InvestmentChangeGrid.ColumnDefinitions.Add(col3I);

                InvestmentChangeGrid.Children.Add(textBlockChangeString);
                InvestmentChangeGrid.Children.Add(textBlockChange);

                Grid.SetColumn(textBlockChangeString, 0);
                Grid.SetColumn(textBlockChange, 1);
                Grid.SetColumnSpan(textBlockChange, 2);


                stackPanel.Children.Add(InvestmentChangeGrid);


                //Stocks amount grid
                TextBlock textBlockStocksString = new TextBlock
                {
                    Text = ResourceLoader.GetForViewIndependentUse().GetString("GeneralStocks") + ": "
                };

                TextBlock textBlockStocks = new TextBlock();
                textBlockStocks.SetBinding(TextBlock.TextProperty, new Binding() { Path = new PropertyPath("Stocks"), Source = investmentData, Mode = BindingMode.OneWay });

                Grid StockGrid = new Grid();
                ColumnDefinition col1S = new ColumnDefinition();
                ColumnDefinition col2S = new ColumnDefinition();
                ColumnDefinition col3S = new ColumnDefinition();
                col1S.Width = new GridLength(1, GridUnitType.Star);
                col2S.Width = new GridLength(1, GridUnitType.Star);
                col3S.Width = new GridLength(1, GridUnitType.Star);

                StockGrid.ColumnDefinitions.Add(col1S);
                StockGrid.ColumnDefinitions.Add(col2S);
                StockGrid.ColumnDefinitions.Add(col3S);

                StockGrid.Children.Add(textBlockStocksString);
                StockGrid.Children.Add(textBlockStocks);

                Grid.SetColumn(textBlockStocksString, 0);
                Grid.SetColumn(textBlockStocks, 1);
                Grid.SetColumnSpan(textBlockStocks, 2);

                stackPanel.Children.Add(StockGrid);


                //Add remove buttons
                Button addButton = new Button
                {
                    Content = ResourceLoader.GetForViewIndependentUse().GetString("GeneralAdd")
                };
                addButton.Click += AddButton_Click;
                addButton.HorizontalAlignment = HorizontalAlignment.Stretch;

                Button removeButton = new Button
                {
                    Content = ResourceLoader.GetForViewIndependentUse().GetString("GeneralRemove")
                };
                removeButton.Click += RemoveButton_Click;
                removeButton.HorizontalAlignment = HorizontalAlignment.Stretch;

                Grid buttonsGrid = new Grid();
                ColumnDefinition col1 = new ColumnDefinition();
                ColumnDefinition col2 = new ColumnDefinition();
                col1.Width = new GridLength(1, GridUnitType.Star);
                col2.Width = new GridLength(1, GridUnitType.Star);

                buttonsGrid.ColumnDefinitions.Add(col1);
                buttonsGrid.ColumnDefinitions.Add(col2);

                buttonsGrid.Children.Add(addButton);
                buttonsGrid.Children.Add(removeButton);

                Grid.SetColumn(addButton, 0);
                Grid.SetColumn(removeButton, 1);

                stackPanel.Children.Add(buttonsGrid);

                border.Child = stackPanel;

                InvestmentDialogStackPanel.Children.Add(border);

                ContentDialogResult result = await InvestmentDialog.ShowAsync();

                if (result == ContentDialogResult.Primary)
                {
                    //SaveAsync
                    investmentData.DateTime = DateTime.Now;
                    App.WebStockDatas.alphaVantage.historylist.ElementAt(dataGridSelectedIndex).ElementAt(0).InvestedStocks = investmentData.Stocks;
                    App.Player.PlayerAmountOnline -= investmentData.Change;
                    App.Player.NewInvestment(new OnlineStockDataInvestment(investmentData));
                    InvestmentDialogStackPanel.Children.Clear();
                    App.WebStockDatas.alphaVantage.SaveAsync();
                }
                else if (result == ContentDialogResult.Secondary)
                {
                    //Secondary
                }
                else
                {
                    //Close button
                    InvestmentDialogStackPanel.Children.Clear();

                }

            } catch
            {

            }

        }
       
        /// <summary>
        /// Add stocks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            var stateShift = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Shift);
            bool shift = (stateShift & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;

            var stateControl = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Control);
            bool control = (stateControl & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;
            if(shift && control)
            {
                //egyszerre
            } else if(shift)
            {
                investmentData.IncreaseStocks(10);

            } else if (control)
            {
                investmentData.IncreaseStocks(50);
            }
            else
            {
                investmentData.IncreaseStocks(1);
            }
        }

        /// <summary>
        /// Decrease stocks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            var stateShift = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Shift);
            bool shift = (stateShift & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;

            var stateControl = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Control);
            bool control = (stateControl & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;
            if (shift && control)
            {
                //egyszerre
            }
            else if (shift)
            {
                investmentData.DecreaseStocks(10);

            }
            else if (control)
            {
                investmentData.DecreaseStocks(50);
            }
            else
            {
                investmentData.DecreaseStocks(1);
            }
        }

    }
}
