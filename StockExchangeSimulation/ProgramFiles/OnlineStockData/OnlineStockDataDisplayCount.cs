﻿using System;

namespace StockExchangeSimulation
{
    public class OnlineStockDataDisplayCount
    {
        public System.Collections.ObjectModel.ObservableCollection<int> webStocksDisplayCount = new System.Collections.ObjectModel.ObservableCollection<int>();

        /// <summary>
        /// Constructor
        /// </summary>
        public OnlineStockDataDisplayCount()
        {
            webStocksDisplayCount.Add(10);
            webStocksDisplayCount.Add(25);
            webStocksDisplayCount.Add(50);
            webStocksDisplayCount.Add(100);
        }
    }
}
