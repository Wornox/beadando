﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace StockExchangeSimulation
{
    /// <summary>
    /// Home page
    /// </summary>
    public sealed partial class HomePage : Page
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public HomePage()
        {
            this.InitializeComponent();
            ReloadString();
        }

        /// <summary>
        /// Reaload string when language changed
        /// </summary>
        private void ReloadString()
        {
            HomePageText.Text = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePageText));


            HomePagePivotMenu.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePagePivotMenu));
            HomePagePivotPlayerStats.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePagePivotPlayerStats));
            HomePagePivotLocalDatabase.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePagePivotLocalDatabase));
            HomePagePivotOnlineDatabase.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePagePivotOnlineDatabase));
            HomePagePivotOfflineDatabase.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePagePivotOfflineDatabase));
            HomePagePivotSettingsDatabase.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePagePivotSettingsDatabase));

            HomePageMenuText.Text = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePageMenuText));
            HomePagePlayerStatsText.Text = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePagePlayerStatsText));
            HomePageLocalDatabaseText.Text = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePageLocalDatabaseText));
            HomePageOnlineDatabaseText.Text = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePageOnlineDatabaseText));
            HomePageOfflineDatabaseText.Text = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePageOfflineDatabaseText));
            HomePageSettingsText.Text = ResourceLoader.GetForViewIndependentUse().GetString(nameof(HomePageSettingsText));

        }
    }
}
