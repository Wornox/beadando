﻿using Microsoft.Toolkit.Uwp.Helpers;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace StockExchangeSimulation
{
    public class LocalStockDataSaveLoad
    {
        LocalObjectStorageHelper SaveLoadhelper = new LocalObjectStorageHelper();
        readonly string SaveLoadhelperKey = "localData";

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {
            try
            {
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKey, App.CompanyStockDataList);
            }
            catch
            {
                //ha éppen foglalt a file, akkor excaptiont dob
            }
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <returns></returns>
        public async Task LoadAsync()
        {
            if (await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKey) //&& await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyHistory)
                )
            {
                ObservableCollection<LocalStockData> tempList = await SaveLoadhelper.ReadFileAsync<ObservableCollection<LocalStockData>>(SaveLoadhelperKey);
                App.CompanyStockDataList = tempList;
            }
            else
            {
                //ha még nem volt lementve
                FillLocalDataList();
            }
        }

        /// <summary>
        /// Fill up database
        /// </summary>
        private void FillLocalDataList()
        {
            App.CompanyStockDataList = new ObservableCollection<LocalStockData>
                {
                    new LocalStockData
                    {
                        CompanyId = 1, CompanyName = "Affle", Description="Computer Industry", CurrentValue = 300, AvailableStocks = 60, CurrentReputation = Reputation.ReputationNeutral, Investment = 0, StockChange = 0, Date = DateTime.Now, CurrentEvent = Event.EventNeutral, CurrentEventDescription = EventDescription.EventDescriptionNeutral
                    },
                    new LocalStockData
                    {
                        CompanyId = 2, CompanyName = "Micronorft", Description="Micro - norft ", CurrentValue = 500, AvailableStocks = 70, CurrentReputation = Reputation.ReputationNeutral, Investment = 0, StockChange = 0, Date = DateTime.Now, CurrentEvent = Event.EventNeutral, CurrentEventDescription = EventDescription.EventDescriptionNeutral
                    },
                    new LocalStockData
                    {
                        CompanyId = 3, CompanyName = "Generation CEO", Description="For the New Generation", CurrentValue = 600, AvailableStocks = 55, CurrentReputation = Reputation.ReputationNeutral, Investment = 0, StockChange = 0, Date = DateTime.Now, CurrentEvent = Event.EventNeutral, CurrentEventDescription = EventDescription.EventDescriptionNeutral
                    },
                    new LocalStockData
                    {
                        CompanyId = 4, CompanyName = "Nekker rt.", Description="You will Nekker", CurrentValue = 400, AvailableStocks = 30, CurrentReputation = Reputation.ReputationNeutral, Investment = 0, StockChange = 0, Date = DateTime.Now, CurrentEvent = Event.EventNeutral, CurrentEventDescription = EventDescription.EventDescriptionNeutral
                    },
                    new LocalStockData
                    {
                        CompanyId = 5, CompanyName = "Ragda Zrt.", Description="Ragda nor ragga", CurrentValue = 200, AvailableStocks = 20, CurrentReputation = Reputation.ReputationNeutral, Investment = 0, StockChange = 0, Date = DateTime.Now, CurrentEvent = Event.EventNeutral, CurrentEventDescription = EventDescription.EventDescriptionNeutral
                    },
                    new LocalStockData
                    {
                        CompanyId = 6, CompanyName = "Hallo KFT.", Description="Hallo, Hallo", CurrentValue = 550, AvailableStocks = 35, CurrentReputation = Reputation.ReputationNeutral, Investment = 0, StockChange = 0, Date = DateTime.Now, CurrentEvent = Event.EventNeutral, CurrentEventDescription = EventDescription.EventDescriptionNeutral
                    },
                    new LocalStockData
                    {
                        CompanyId = 7, CompanyName = "World Stock rt.", Description="We are watching", CurrentValue = 450, AvailableStocks = 45, CurrentReputation = Reputation.ReputationNeutral, Investment = 0, StockChange = 0, Date = DateTime.Now, CurrentEvent = Event.EventNeutral, CurrentEventDescription = EventDescription.EventDescriptionNeutral
                    },
                    new LocalStockData
                    {
                        CompanyId = 8, CompanyName = "Nao", Description="Nao genesis", CurrentValue = 650, AvailableStocks = 50, CurrentReputation = Reputation.ReputationNeutral, Investment = 0, StockChange = 0, Date = DateTime.Now, CurrentEvent = Event.EventNeutral, CurrentEventDescription = EventDescription.EventDescriptionNeutral
                    }
                };

            /*
            //Adding the first(static) datas to History
            ObservableCollection<LocalStockData> newList = new ObservableCollection<LocalStockData>();

            foreach (var element in App.CompanyStockDataList)
            {
                LocalStockData newData = new LocalStockData(element);
                newList.Add(newData);
            }

            App.CompanyStockDataHistory.AddNewHistory(newList);

            */
            /*
            //Graph
            GraphData apple = new GraphData();
            apple.DateTime = App.CompanyStockDataList.ElementAt(0).Date;
            apple.Value = App.CompanyStockDataList.ElementAt(0).CurrentValue;
            App.AppleGraphView.Add(apple);

            GraphData mic = new GraphData();
            mic.DateTime = App.CompanyStockDataList.ElementAt(1).Date;
            mic.Value = App.CompanyStockDataList.ElementAt(1).CurrentValue;
            App.MicrosoftGraphView.Add(mic);

            GraphData gen = new GraphData();
            gen.DateTime = App.CompanyStockDataList.ElementAt(2).Date;
            gen.Value = App.CompanyStockDataList.ElementAt(2).CurrentValue;
            App.GenerationGraphView.Add(gen);

            GraphData nekk = new GraphData();
            nekk.DateTime = App.CompanyStockDataList.ElementAt(3).Date;
            nekk.Value = App.CompanyStockDataList.ElementAt(3).CurrentValue;
            App.NekkerGraphView.Add(nekk);


            ObservableCollection<GraphData> newgraphdata = new ObservableCollection<GraphData>();

            foreach (var element in App.CompanyStockDataList)
            {
                GraphData graphData = new GraphData();
                graphData.DateTime = element.Date;
                graphData.Value = element.CurrentValue;
                newgraphdata.Add(graphData);
            }

            App.GraphDataList.Add(newgraphdata);
            */
        }
    }
}
