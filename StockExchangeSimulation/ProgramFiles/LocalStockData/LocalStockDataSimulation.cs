﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;

namespace StockExchangeSimulation
{

    public class LocalStockDataSimulation
    {
        DispatcherTimer dispatcherTimer;

        /// <summary>
        /// Constructor
        /// </summary>
        public LocalStockDataSimulation()
        {
            Timer();
        }

        /// <summary>
        /// Timer setup
        /// </summary>
        private void Timer()
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 5);
            dispatcherTimer.Start();
        }

        /// <summary>
        /// Timer tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dispatcherTimer_Tick(object sender, object e)
        {
            CalculateDataAsync();
        }

        /// <summary>
        /// Refresh comapny datas
        /// </summary>
        private async void CalculateDataAsync()
        {
            //Set progressBar timing
            App.Refreshprogressbar._lastTimeRefresh = DateTime.Now;

            App.Refreshprogressbar._nextTimeRefresh = DateTime.Now + TimeSpan.FromSeconds(5);

            //History
            ObservableCollection<LocalStockData> newHistory = new ObservableCollection<LocalStockData>();
            foreach (var element in App.CompanyStockDataList)
            {
                LocalStockData data = new LocalStockData(element);
                newHistory.Add(data);
            }
            App.CompanyStockDataHistory.AddNewHistory(newHistory);


            //Check for Event expire
            foreach (var element in App.CompanyStockDataList)
            {
                if (element.CurrentEventEndTime <= DateTime.Now)
                {
                    element.CurrentEvent = Event.EventNeutral;
                }
            }

            //RegularChange
            Random randRegularChange = new Random();

            foreach (var element in App.CompanyStockDataList)
            {
                if (element.CurrentEvent > 0)
                {
                    element.StockChange = randRegularChange.Next(0, 10);
                    element.CurrentValue += element.StockChange;
                }
                else if (element.CurrentEvent < 0)
                {
                    element.StockChange = randRegularChange.Next(-10, 0);
                    if((element.CurrentValue + element.StockChange) < 0) { element.StockChange = 0;}
                    element.CurrentValue += element.StockChange;

                }
                else
                {
                    element.StockChange = randRegularChange.Next(-10, 10);
                    if ((element.CurrentValue + element.StockChange) < 0) { element.StockChange = 0; }
                    element.CurrentValue += element.StockChange;
                }

                element.Date = DateTime.Now;

                if (element.StockChange > 7) element.CurrentReputation = Reputation.ReputationVeryGood;
                else if (element.StockChange > 3) element.CurrentReputation = Reputation.ReputationGood;
                else if (element.StockChange < -7) element.CurrentReputation = Reputation.ReputationVeryBad;
                else if (element.StockChange < -3) element.CurrentReputation = Reputation.ReputationBad;
                else element.CurrentReputation = Reputation.ReputationNeutral;

                //investment recalculate
                element.Investment = element.OwnedStocks * element.CurrentValue;
            }

            //Adding datas to History
            ObservableCollection<LocalStockData> newList = new ObservableCollection<LocalStockData>();

            foreach (var element in App.CompanyStockDataList)
            {
                LocalStockData newData = new LocalStockData(element);
                newList.Add(newData);
            }

            App.CompanyStockDataHistory.AddNewHistory(newList);

            //add graph history
            int index = 0;
            foreach(var element in App.CompanyStockDataList)
            {
                LocalStockDataGraphViewModel.GraphData gd = new LocalStockDataGraphViewModel.GraphData();
                gd.DateTime = App.CompanyStockDataList.ElementAt(index).Date;
                gd.Value = App.CompanyStockDataList.ElementAt(index).CurrentValue;
                App.GraphDataList.GraphDataList.ElementAt(index).Add(gd);
                if (App.GraphDataList.GraphDataList.ElementAt(index).Count > 30) App.GraphDataList.GraphDataList.ElementAt(index).RemoveAt(0);
                index++;
            }

            //Big Event
            Random randBigEventPossiblity = new Random();
            int bigEventPossiblity = randBigEventPossiblity.Next(1, 5);

            if (bigEventPossiblity == 1)
            {
                List<LocalStockData> bigEventList = new List<LocalStockData>();

                foreach (var element in App.CompanyStockDataList)
                {
                    if (element.CurrentReputation != Reputation.ReputationNeutral && element.CurrentEvent == Event.EventNeutral) bigEventList.Add(element);
                }

                if (bigEventList.Count != 0)
                {
                    int bigEventCompany;

                    Random randCompanyID = new Random();

                    bigEventCompany = randCompanyID.Next(0, bigEventList.Count - 1);

                    LocalStockData BigEventCompany = bigEventList.ElementAt(bigEventCompany);

                    Random randEvent = new Random();

                    double eventSec = 0;

                    if (BigEventCompany.StockChange > 0)
                    {
                        int rand = randEvent.Next(1, 2);
                        BigEventCompany.CurrentEvent = (Event)rand;
                        BigEventCompany.CurrentEventDescription = (EventDescription)rand;
                        if (rand == 1) eventSec = 20;
                        if (rand == 2) eventSec = 30;
                    }
                    else if (BigEventCompany.StockChange < 0)
                    {
                        int rand = randEvent.Next(-2, -1);
                        BigEventCompany.CurrentEvent = (Event)rand;
                        BigEventCompany.CurrentEventDescription = (EventDescription)rand;
                        if (rand == -1) eventSec = 20;
                        if (rand == -2) eventSec = 30;
                    }
                    else
                    {
                        BigEventCompany.CurrentEvent = Event.EventNeutral;
                    }

                    BigEventCompany.CurrentEventEndTime = DateTime.Now;
                    BigEventCompany.CurrentEventEndTime = BigEventCompany.CurrentEventEndTime.AddSeconds(eventSec);

                    LocalStockData newBigEvent = new LocalStockData(BigEventCompany);

                    App.CompanyStockDataHistory.AddNewEvent(newBigEvent);
                    if (App.CompanyStockDataHistory.events.Count > 30) App.CompanyStockDataHistory.events.RemoveAt(0);
                }
            }

            App.CompanyStockDataSaveLoad.SaveAsync();

        }

    }
}
