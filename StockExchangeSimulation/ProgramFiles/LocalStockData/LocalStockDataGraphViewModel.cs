﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockExchangeSimulation
{
    public class LocalStockDataGraphViewModel
    {
        /// <summary>
        /// Graph data view class
        /// </summary>
        public class GraphData
        {
            public DateTime DateTime { get; set; }
            public int Value { get; set; }
        }

        /// <summary>
        /// Data list
        /// </summary>
        public ObservableCollection<ObservableCollection<GraphData>> GraphDataList = new ObservableCollection<ObservableCollection<GraphData>>();

        /// <summary>
        /// Constructor
        /// </summary>
        public LocalStockDataGraphViewModel()
        {
            foreach (var element in App.CompanyStockDataList)
            {
                GraphDataList.Add(new ObservableCollection<GraphData>());
            }
        }

        /// <summary>
        /// Add the new graph data to the list
        /// </summary>
        /// <param name="newgraphdata">complate</param>
        public void Add(ObservableCollection<GraphData> newgraphdata)
        {
            GraphDataList.Add(newgraphdata);
        }
    }
}
