﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml;

namespace StockExchangeSimulation
{
    public class LocalStockDataProgressBar : INotifyPropertyChanged
    {
        DispatcherTimer dispatcherTimer;

        public DateTime _lastTimeRefresh;
        public DateTime _nextTimeRefresh;

        public double _value;
        public double Value
        {
            get { return _value; }
            set
            {
                this._value = value;
                RaisePropertyChanged("Value");
            }
        }

        public int _range;
        public int Range
        {
            get { return _range; }
            set
            {
                this._range = value;
                RaisePropertyChanged("Range");
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="refreshtime"></param>
        public LocalStockDataProgressBar(int refreshtime)
        {
            _lastTimeRefresh = DateTime.Now;
            Value = 0;
            _range = refreshtime;
            DispatcherTimerSetup();
        }

        /// <summary>
        /// Timer setup
        /// </summary>
        public void DispatcherTimerSetup()
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = TimeSpan.FromMilliseconds(1);
            dispatcherTimer.Start();
        }

        /// <summary>
        /// Timer tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void dispatcherTimer_Tick(object sender, object e)
        {
            Value = (DateTime.Now - _lastTimeRefresh).TotalMilliseconds / (double)1000;
        }

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }
}
