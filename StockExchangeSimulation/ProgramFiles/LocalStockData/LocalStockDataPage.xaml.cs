﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Data;
using System.Linq;
using System.Collections.ObjectModel;

namespace StockExchangeSimulation
{
    public class GraphConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var e = (ObservableCollection<ObservableCollection<LocalStockData>>)value;
            ObservableCollection<LocalStockData> list = new ObservableCollection<LocalStockData>();
            if (e.Count > 0)
            {
                switch ((string)parameter)
                {
                    case "apple":
                        foreach(var element in e)
                        {
                            LocalStockData lsd = new LocalStockData(element.ElementAt(0));
                            list.Add(lsd);

                        }
                        break;
                    case "high":
                        break;
                    case "low":
                        break;
                    case "close":
                        break;
                }

            }
            return list;
        }

        // ConvertBack is not implemented for a OneWay binding.
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Local page
    /// </summary>
    public sealed partial class LocalStockDataPage : Page
    {
        static bool EventSwitch = false;
        static bool EventCanBeDisplayed = true;
        List<Popup> popupList = new List<Popup>();

        /// <summary>
        /// Constructor
        /// </summary>
        public LocalStockDataPage()
        {
            this.InitializeComponent();

            EventPopupSwitch.IsOn = EventSwitch;

            TableViewPivot.Header = ResourceLoader.GetForViewIndependentUse().GetString("PivotTableView");
            BarViewPivot.Header = ResourceLoader.GetForViewIndependentUse().GetString("PivotBarView");
            GraphViewPivot.Header = ResourceLoader.GetForViewIndependentUse().GetString("PivotGraphView");

            DataGridColumnCompanyName.Header = ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnCompanyName");
            DataGridColumnCurrentValue.Header = ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnCurrentValue");
            DataGridColumnAvailableStocks.Header = ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnAvailableStocks");
            DataGridColumnCurrentReputation.Header = ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnCurrentReputation");
            DataGridColumnStockChange.Header = ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnStockChange");
            DataGridColumnCurrentEvent.Header = ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnCurrentEvent");
            DataGridColumnInvestment.Header = ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnInvestment");
            DataGridColumnOwnedStocks.Header = ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnOwnedStocks");

            InvestmentDialog.Title = ResourceLoader.GetForViewIndependentUse().GetString("VariableInvestmentManager");
            InvestmentDialog.PrimaryButtonText = ResourceLoader.GetForViewIndependentUse().GetString("VariableInvestmentManagerPrimaryButton");
            InvestmentDialog.CloseButtonText = ResourceLoader.GetForViewIndependentUse().GetString("VariableInvestmentManagerCloseButton");

        }

        /// <summary>
        /// Event list datas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            object obj = (sender as ListView).SelectedItem;

            LocalStockData temp = obj as LocalStockData;

            if(temp != null)
            {
                EventDetailRun.Text = temp.EventHistoryToString();

                if (temp.CurrentEvent == Event.EventAccountSuccess) EventDetailImage.Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri("ms-appx:///Assets/AccSuccess.png"));
                if (temp.CurrentEvent == Event.EventAccountMistake) EventDetailImage.Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri("ms-appx:///Assets/AccMistake.png"));
                if (temp.CurrentEvent == Event.EventEngineerSuccess) EventDetailImage.Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri("ms-appx:///Assets/EngSuccess.png"));
                if (temp.CurrentEvent == Event.EventEngineerMistake) EventDetailImage.Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri("ms-appx:///Assets/EngMistake.png"));
            } else
            {
                EventDetailRun.Text = "";
                EventDetailImage.Source = null;
            }
        }

        /// <summary>
        /// Invest to company in the tablegrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Grid_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            try
            {
                object s = (sender as Telerik.UI.Xaml.Controls.Grid.RadDataGrid).SelectedItem;

                LocalStockData temp = s as LocalStockData;
                int avalibleStock = temp.AvailableStocks;
                int currentValue = temp.CurrentValue;
                int oldInvestment = temp.Investment;
                int ownedStocks = temp.OwnedStocks;
                double money = App.Player.PlayerAmountLocal;

                int maxBuyableStocks = (int)money / currentValue;

                InvestSlider.TickFrequency = 1;  //stockonként lehet beruházni

                InvestSlider.Maximum = ownedStocks + Math.Min(avalibleStock, maxBuyableStocks);
                InvestSlider.Value = ownedStocks;
                InvestSlider.Minimum = 0;

                InfoBox.Text = 
                    ResourceLoader.GetForViewIndependentUse().GetString("VariableCompanyName") + "\n\t" + temp.CompanyName + "\n" +
                    ResourceLoader.GetForViewIndependentUse().GetString("VariableReputation") + "\n\t" + temp.CurrentReputationTranslated + "\n" +
                    ResourceLoader.GetForViewIndependentUse().GetString("VariableAvalibleStocks") + "\n\t" + avalibleStock + "\n" +
                    ResourceLoader.GetForViewIndependentUse().GetString("VariableCurrentValue") + "\n\t" + currentValue + "\n" +
                    ResourceLoader.GetForViewIndependentUse().GetString("VariableOwnedStocks") + "\n\t" + ownedStocks + "\n" +
                    ResourceLoader.GetForViewIndependentUse().GetString("VariableOldInvestment") + "\n\t" + oldInvestment;

                ContentDialogResult result = await InvestmentDialog.ShowAsync();

                if (result == ContentDialogResult.Primary)
                {

                    int decidedStocks = (int)InvestSlider.Value;
                    int stockdiff = ownedStocks - decidedStocks;
                    temp.AvailableStocks += stockdiff;
                    temp.OwnedStocks = decidedStocks;
                    App.Player.PlayerAmountLocal += stockdiff * currentValue;

                    temp.Investment = temp.OwnedStocks * temp.CurrentValue;
                    var newdata = new LocalStockData(temp);
                    App.CompanyStockDataHistory.AddNewInvestment(newdata);
                    App.CompanyStockDataSaveLoad.SaveAsync();
                    App.Player.SaveAsync();
                }
                else if (result == ContentDialogResult.Secondary)
                {
                    //Secondary
                }
                else
                {
                    //Close button
                }

            }
            catch
            {
                //az ablak létrehozásakor hiba
            }

        }

        /// <summary>
        /// Create popup window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void ListViewHistory_ContainerContentChanging(ListViewBase sender, ContainerContentChangingEventArgs args)
        {
            if (EventSwitch && EventCanBeDisplayed)
            {
                LocalStockData data = args.Item as LocalStockData;

                    Popup popup = new Popup();
                        Border border = new Border();
                        border.BorderBrush = new SolidColorBrush(Windows.UI.Colors.Gray);
                        border.BorderThickness = new Thickness() { Bottom = 2, Left = 2, Right = 2, Top = 2 };
                        var newBackground = Application.Current.Resources["ApplicationPageBackgroundThemeBrush"] as SolidColorBrush;
                        if(newBackground != null) border.Background = newBackground;

                             StackPanel stackPanel = new StackPanel();

                                RichTextBlock info = new RichTextBlock();

                                    Run run = new Run();
                                    run.Text = data.EventHistoryToString();

                                    Paragraph paragraph = new Paragraph();
                                    paragraph.Inlines.Add(run);

                                info.Height = 200;
                                info.Width = 600;
                                info.Blocks.Add(paragraph);

                            stackPanel.Children.Add(info);

                                Button closeButton = new Button();
                                closeButton.Content = ResourceLoader.GetForViewIndependentUse().GetString("VariableClose");
                                closeButton.Click += CloseButton_Click;
                                closeButton.HorizontalAlignment = HorizontalAlignment.Stretch;   

                                Button closeAllButton = new Button();
                                closeAllButton.Content = ResourceLoader.GetForViewIndependentUse().GetString("VariableCloseAll");
                                closeAllButton.Click += CloseAllButton_Click;
                                closeAllButton.HorizontalAlignment = HorizontalAlignment.Stretch;

                                Grid buttonsGrid = new Grid();
                                ColumnDefinition col1 = new ColumnDefinition();
                                ColumnDefinition col2 = new ColumnDefinition();
                                col1.Width = new GridLength(1, GridUnitType.Star);
                                col2.Width = new GridLength(1, GridUnitType.Star);

                                buttonsGrid.ColumnDefinitions.Add(col1);
                                buttonsGrid.ColumnDefinitions.Add(col2);

                                buttonsGrid.Children.Add(closeButton);
                                buttonsGrid.Children.Add(closeAllButton);

                                Grid.SetColumn(closeButton, 0);
                                Grid.SetColumn(closeAllButton, 1);

                            stackPanel.Children.Add(buttonsGrid);

                        border.Child = stackPanel;

                    popup.IsOpen = true;
                    popup.Child = border;

                popupList.Add(popup);

                MainGrid.Children.Add(popup);
            }
        }

        /// <summary>
        /// Close top popup window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            var a = sender as FrameworkElement;

            while (a.Parent != null && !(a.Parent is Popup))
            {
                a = a.Parent as FrameworkElement;
            }

            Popup popup = a.Parent as Popup;
            popup.IsOpen = false;
            popupList.Remove(popup);
        }

        /// <summary>
        /// Close all popup windows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseAllButton_Click(object sender, RoutedEventArgs e)
        {
            foreach(var element in popupList)
            {
                element.IsOpen = false;
            }
            popupList.Clear();
        }

        /// <summary>
        /// Enable or disable popup windwos toggle switch
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventPopupSwitch_Toggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn == true)
                {
                    EventSwitch = true;
                }
                else
                {
                    EventSwitch = false;
                }
            }
        }

        /// <summary>
        /// Set EventCanBeDisplayed to true when loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListViewHistory_Loaded(object sender, RoutedEventArgs e)
        {
            EventCanBeDisplayed = true;
        }

        /// <summary>
        /// Graph mouse cursor data display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChartTrackBallBehavior_TrackInfoUpdated(object sender, Telerik.UI.Xaml.Controls.Chart.TrackBallInfoEventArgs e)
        {
            e.Context.ClosestDataPoint.DisplayHeader = "";
        }

    }
}
