﻿using Microsoft.Toolkit.Uwp.Helpers;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace StockExchangeSimulation
{
    public class LocalStockDataHistory
    {
        /// <summary>
        /// Data store lists
        /// </summary>
        public ObservableCollection<ObservableCollection<LocalStockData>> history;
        public ObservableCollection<LocalStockData> investment;
        public ObservableCollection<LocalStockData> events;

        /// <summary>
        /// Save and Load JSON files
        /// </summary>
        LocalObjectStorageHelper SaveLoadhelper = new LocalObjectStorageHelper();
        readonly string SaveLoadhelperKeyInvestment = "localDataInvestment";

        /// <summary>
        /// Constructor
        /// </summary>
        public LocalStockDataHistory()
        {
            history = new ObservableCollection<ObservableCollection<LocalStockData>>();
            investment = new ObservableCollection<LocalStockData>();
            events = new ObservableCollection<LocalStockData>();
        }

        /// <summary>
        /// Add company to history list
        /// </summary>
        /// <param name="data"></param>
        public void AddNewHistory(ObservableCollection<LocalStockData> data)
        {
            history.Add(data);
        }

        /// <summary>
        /// Add new investment to investment list
        /// </summary>
        /// <param name="data"></param>
        public void AddNewInvestment(LocalStockData data)
        {
            investment.Add(data);
            SaveAsync();
        }

        /// <summary>
        /// Add new event to event list
        /// </summary>
        /// <param name="data"></param>
        public void AddNewEvent(LocalStockData data)
        {
            events.Add(data);
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {
            try
            {
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyInvestment, investment);
            }
            catch
            {
                //ha éppen foglalt a file, akkor excaptiont dob
            }
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <returns></returns>
        public async Task LoadAsync()
        {
            if (await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyInvestment))
            {
                ObservableCollection<LocalStockData> tempInvestment = await SaveLoadhelper.ReadFileAsync<ObservableCollection<LocalStockData>>(SaveLoadhelperKeyInvestment);
                foreach(var element in tempInvestment)
                {
                    investment.Add(element);
                }
            }
            else
            {
                //semmi
            }
        }
    }
}
