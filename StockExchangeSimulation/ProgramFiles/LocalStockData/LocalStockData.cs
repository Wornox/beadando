﻿using System;
using System.ComponentModel;
using Windows.ApplicationModel.Resources;

namespace StockExchangeSimulation
{
    public enum Reputation { ReputationVeryBad, ReputationBad, ReputationNeutral, ReputationGood, ReputationVeryGood }

    public enum Event { EventEngineerMistake = -2, EventAccountMistake = -1, EventNeutral = 0, EventAccountSuccess = 1, EventEngineerSuccess = 2 }

    public enum EventDescription { EventDescriptionEngineerMistake = -2, EventDescriptionAccountMistake = -1, EventDescriptionNeutral = 0, EventDescriptionAccountSuccess = 1, EventDescriptionEngineerSuccess = 2 }

    public class LocalStockData : INotifyPropertyChanged
    {
        #region Properties
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }

        private int _CurrentValue;
        public int CurrentValue
        {
            get { return _CurrentValue; }
            set
            {
                this._CurrentValue = value;
                RaisePropertyChanged("CurrentValue");
            }
        }

        private int _AvailableStocks;
        public int AvailableStocks
        {
            get { return _AvailableStocks; }
            set
            {
                this._AvailableStocks = value;
                RaisePropertyChanged("AvailableStocks");
            }
        }

        private int _OwnedStocks;
        public int OwnedStocks
        {
            get { return _OwnedStocks; }
            set
            {
                this._OwnedStocks = value;
                RaisePropertyChanged("OwnedStocks");
            }
        }

        private int _Investment;
        public int Investment
        {
            get { return _Investment; }
            set
            {
                this._Investment = value;
                RaisePropertyChanged("Investment");
            }
        }


        private int _StockChange;
        public int StockChange
        {
            get { return _StockChange; }
            set
            {
                this._StockChange = value;
                RaisePropertyChanged("StockChange");
            }
        }


        private Reputation _CurrentReputation;
        private string _CurrentReputationTranslated;
        public Reputation CurrentReputation
        {
            get { return _CurrentReputation; }
            set
            {
                this._CurrentReputation = value;
                this._CurrentReputationTranslated = ResourceLoader.GetForViewIndependentUse().GetString(_CurrentReputation.ToString());
                RaisePropertyChanged("CurrentReputation");
            }
        }
        public string CurrentReputationTranslated
        {
            get { return _CurrentReputationTranslated; }
            set
            {
                this._CurrentReputationTranslated = value;
                RaisePropertyChanged("CurrentReputationTranslated");
            }
        }


        private Event _CurrentEvent;
        private string _CurrentEventTranslated;
        public Event CurrentEvent
        {
            get { return _CurrentEvent; }
            set
            {
                this._CurrentEvent = value;
                this._CurrentEventTranslated = ResourceLoader.GetForViewIndependentUse().GetString(_CurrentEvent.ToString());
                RaisePropertyChanged("CurrentEvent");
            }
        }
        public string CurrentEventTranslated
        {
            get { return _CurrentEventTranslated; }
            set
            {
                this._CurrentEventTranslated = value;
                RaisePropertyChanged("CurrentEventTranslated");
            }
        }

        private EventDescription _CurrentEventDescription;
        private string _CurrentEventDescriptionTranslated;
        public EventDescription CurrentEventDescription
        {
            get { return _CurrentEventDescription; }
            set
            {
                this._CurrentEventDescription = value;
                this._CurrentEventDescriptionTranslated = ResourceLoader.GetForViewIndependentUse().GetString(_CurrentEventDescription.ToString());
                RaisePropertyChanged("CurrentEventDescription");
            }
        }
        public string CurrentEventDescriptionTranslated
        {
            get { return _CurrentEventDescriptionTranslated; }
            set
            {
                this._CurrentEventDescriptionTranslated = value;
                RaisePropertyChanged("CurrentEventDescriptionTranslated");
            }
        }


        private DateTime _CurrentEventEndTime;
        public DateTime CurrentEventEndTime
        {
            get { return _CurrentEventEndTime; }
            set
            {
                this._CurrentEventEndTime = value;
                RaisePropertyChanged("CurrentEventEndTime");
            }
        }

        private DateTime _Date;
        public DateTime Date
        {
            get { return _Date; }
            set
            {
                this._Date = value;
                RaisePropertyChanged("Date");
            }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public LocalStockData()
        {

        }

        /// <summary>
        /// Create a copy of the object
        /// </summary>
        /// <param name="other">the object from copy to</param>
        public LocalStockData(LocalStockData other)
        {
            CompanyId = other.CompanyId;
            CompanyName = other.CompanyName;
            Description = other.Description;
            CurrentValue = other.CurrentValue;
            Investment = other.Investment;
            CurrentReputation = other.CurrentReputation;
            CurrentEvent = other.CurrentEvent;
            CurrentEventEndTime = other.CurrentEventEndTime;
            CurrentEventDescription = other.CurrentEventDescription;
            StockChange = other.StockChange;
            Date = other.Date;
            AvailableStocks = other.AvailableStocks;
            OwnedStocks = other.OwnedStocks;
        }

        /// <summary>
        /// Return a simple string with the translated data
        /// </summary>
        /// <returns></returns>
        public string EventHistoryToString()
        {
            var res = ResourceLoader.GetForViewIndependentUse();
            return res.GetString("VariableCompanyName") + ":\t" + CompanyName + "\n" +
                    res.GetString("VariableDescription") + ":\t" + Description + "\n" +
                    res.GetString("VariableCurrentEventTranslated") + ":      \t" + res.GetString(CurrentEvent.ToString()) + "\n" +
                    res.GetString("VariableDate") + ":        \t" + Date + "\n" +
                    res.GetString("VariableCurrentEventEndTime") + ":     \t" + CurrentEventEndTime + "\n" +
                    res.GetString("VariableCurrentEventDescriptionTranslated") + ":\t" + res.GetString(CurrentEventDescription.ToString());
        }

        /// <summary>
        /// Override ToString()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var res = ResourceLoader.GetForViewIndependentUse();
            return CompanyName + " " + res.GetString(CurrentEvent.ToString()) + " " + Date;
        }

        #region PropertyChangedEventHandler

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion 
    }

}
