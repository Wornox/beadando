﻿using System;

namespace StockExchangeSimulation
{
    public class OfflineStockDataTimerComboBox
    {
        public System.Collections.ObjectModel.ObservableCollection<int> offlineStockDataTimerComboBox = new System.Collections.ObjectModel.ObservableCollection<int>();

        /// <summary>
        /// Constructor
        /// </summary>
        public OfflineStockDataTimerComboBox()
        {
            offlineStockDataTimerComboBox.Add(1);
            offlineStockDataTimerComboBox.Add(5);
            offlineStockDataTimerComboBox.Add(10);
            offlineStockDataTimerComboBox.Add(30);
            offlineStockDataTimerComboBox.Add(60);
        }
    }
}
