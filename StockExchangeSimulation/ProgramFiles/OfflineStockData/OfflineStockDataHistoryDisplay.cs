﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockExchangeSimulation
{
    public class OfflineStockDataHistoryDisplay
    {
        public ObservableCollection<AlphaVantageOffline.WebData> displayList = new ObservableCollection<AlphaVantageOffline.WebData>();

        public ObservableCollection<AlphaVantageOffline.WebData> list = new ObservableCollection<AlphaVantageOffline.WebData>();

        public int DisplayCountComboBox = -1;

        public int dataGridSelectedIndex = -1;

        public int DisplayCount = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public OfflineStockDataHistoryDisplay()
        {

        }

        /// <summary>
        /// Refresh show datalist to display count
        /// </summary>
        public void Refresh(int DisplayCountComboBox, int dataGridSelectedIndex)
        {

            this.DisplayCountComboBox = DisplayCountComboBox;
            this.dataGridSelectedIndex = dataGridSelectedIndex;

            if (dataGridSelectedIndex >= 0)
            {
                if (App.OfflineStockDatas.alphaVantageOffline.historylist.ElementAt(dataGridSelectedIndex).Count < DisplayCountComboBox)
                {
                    DisplayCount = App.OfflineStockDatas.alphaVantageOffline.historylist.ElementAt(dataGridSelectedIndex).Count;
                }
                else
                {
                    DisplayCount = DisplayCountComboBox;
                }

                displayList.Clear();
                for (int i = 0; i < DisplayCount; i++)
                {
                    displayList.Add(App.OfflineStockDatas.alphaVantageOffline.historylist.ElementAt(dataGridSelectedIndex).ElementAt(i));
                }
            }

        }

        /// <summary>
        /// Refersh data view
        /// </summary>
        public void Refresh()
        {
            if(dataGridSelectedIndex >= 0)
            {
                if (App.OfflineStockDatas.alphaVantageOffline.historylist.ElementAt(dataGridSelectedIndex).Count < DisplayCountComboBox)
                {
                    DisplayCount = App.OfflineStockDatas.alphaVantageOffline.historylist.ElementAt(dataGridSelectedIndex).Count;
                }
                else
                {
                    DisplayCount = DisplayCountComboBox;
                }

                //Eltérés van..... ha csak az elsőben tér el akkor utolsó ki, új be elsó helyre
                if (App.OfflineStockDatas.alphaVantageOffline.historylist.ElementAt(dataGridSelectedIndex).ElementAt(0) != displayList.ElementAt(0))
                {
                    //ha nem csak 1 az eltérés - azaz a régi lista első eleme nem ugyan az mint a új list másodikja
                    if (App.OfflineStockDatas.alphaVantageOffline.historylist.ElementAt(dataGridSelectedIndex).ElementAt(1) != displayList.ElementAt(0))
                    {
                        displayList.Clear();
                        //újra az egész list
                        for (int i = 0; i < DisplayCount; i++)
                        {
                            displayList.Add(App.OfflineStockDatas.alphaVantageOffline.historylist.ElementAt(dataGridSelectedIndex).ElementAt(i));
                        }
                    }
                    else
                    {
                        //csak 1 volt az eltérés ezért beszúrja az 1 új adatot
                        displayList.Insert(0, App.OfflineStockDatas.alphaVantageOffline.historylist.ElementAt(dataGridSelectedIndex).ElementAt(0));
                        //ha túl sok akkor törli az utolsót
                        if(DisplayCount < displayList.Count) displayList.RemoveAt(displayList.Count - 1);
                    }
                }
            }
        }

    }
}
