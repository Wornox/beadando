﻿using System;
using System.Linq;
using Windows.ApplicationModel.Resources;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace StockExchangeSimulation
{
    /// <summary>
    /// Color converter for the datatable
    /// </summary>
    public class ColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var e = (System.Collections.ObjectModel.ObservableCollection<AlphaVantageOffline.WebData>)value;

            var color = new SolidColorBrush(Windows.UI.Colors.Gray);

            if (e.Count >= 2)
            {
                switch((string)parameter)
                {
                    case "open":
                        if (e.ElementAt(0).Open > e.ElementAt(1).Open) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                        else if(e.ElementAt(0).Open < e.ElementAt(1).Open) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                        else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                        break;
                    case "high":
                        if (e.ElementAt(0).High > e.ElementAt(1).High) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                        else if (e.ElementAt(0).High < e.ElementAt(1).High) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                        else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                        break;
                    case "low":
                        if (e.ElementAt(0).Low > e.ElementAt(1).Low) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                        else if (e.ElementAt(0).Low < e.ElementAt(1).Low) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                        else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                        break;
                    case "close":
                        if (e.ElementAt(0).Close > e.ElementAt(1).Close) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                        else if (e.ElementAt(0).Close < e.ElementAt(1).Close) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                        else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                        break;
                }
                
            }
            return color;
        }

        // ConvertBack is not implemented for a OneWay binding.
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Offline page
    /// </summary>
    public sealed partial class OfflineStockDataPage : Page
    {
        public int dataGridSelectedIndex;

        OfflineStockDataInvestment investmentData;

        /// <summary>
        /// Constructor
        /// </summary>
        public OfflineStockDataPage()
        {
            dataGridSelectedIndex = -1;
            this.InitializeComponent();

            DisplayCountComboBox.SelectedItem = App.WebStocksDisplayCount.webStocksDisplayCount.ElementAt(0);

            OfflineTimerComboBox.SelectedItem = App.OfflineStockDataTimerComboBox.offlineStockDataTimerComboBox.ElementAt(1);

            //AlphaVantage Main Table
            DataGridColumnWebStockDataAlphaVantageName.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageName));
            DataGridColumnWebStockDataAlphaVantageSymbol.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageSymbol));
            DataGridColumnWebStockDataAlphaVantageRegion.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageRegion));
            DataGridColumnWebStockDataAlphaVantageMarketOpen.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageMarketOpen));
            DataGridColumnWebStockDataAlphaVantageMarketClose.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageMarketClose));
            DataGridColumnWebStockDataAlphaVantageTimeZone.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageTimeZone));
            DataGridColumnWebStockDataAlphaVantageDateTime.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageDateTime));
            DataGridColumnWebStockDataAlphaVantageCurrency.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageCurrency));
            DataGridColumnWebStockDataAlphaVantageOpen.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageOpen));
            DataGridColumnWebStockDataAlphaVantageHigh.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageHigh));
            DataGridColumnWebStockDataAlphaVantageLow.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageLow));
            DataGridColumnWebStockDataAlphaVantageClose.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageClose));
            DataGridColumnWebStockDataAlphaVantageVolume.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageVolume));
            DataGridColumnWebStockDataAlphaVantageInvestedStocks.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageInvestedStocks));

            //AlphaVantage History Table
            DataGridColumnWebStockDataAlphaVantageDateTimeHistory.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageDateTime));
            DataGridColumnWebStockDataAlphaVantageOpenHistory.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageOpen));
            DataGridColumnWebStockDataAlphaVantageHighHistory.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageHigh));
            DataGridColumnWebStockDataAlphaVantageLowHistory.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageLow));
            DataGridColumnWebStockDataAlphaVantageCloseHistory.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridColumnWebStockDataAlphaVantageClose));

            //Topgrid Buttons
            ButtonPause.Content = ResourceLoader.GetForViewIndependentUse().GetString(nameof(ButtonPause));
            ButtonPlay.Content = ResourceLoader.GetForViewIndependentUse().GetString(nameof(ButtonPlay));
            ButtonRestart.Content = ResourceLoader.GetForViewIndependentUse().GetString(nameof(ButtonRestart));

            OfflinePageCandle.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(OfflinePageCandle));
            OfflinePageLine.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(OfflinePageLine));

            DisplayCountComboBox.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DisplayCountComboBox));
            OfflineTimerComboBox.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(OfflineTimerComboBox));
            SimulatedTimeString.Text = ResourceLoader.GetForViewIndependentUse().GetString(nameof(SimulatedTimeString));

            InvestmentDialog.Title = ResourceLoader.GetForViewIndependentUse().GetString("VariableInvestmentManager");
            InvestmentDialog.PrimaryButtonText = ResourceLoader.GetForViewIndependentUse().GetString("GeneralSave");
            InvestmentDialog.CloseButtonText = ResourceLoader.GetForViewIndependentUse().GetString("GeneralClose");



        }

        /// <summary>
        /// Select company in the table grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadDataGrid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                var data = ((sender as Telerik.UI.Xaml.Controls.Grid.RadDataGrid).SelectedItem) as System.Collections.ObjectModel.ObservableCollection<AlphaVantageOffline.WebData>;

                dataGridSelectedIndex = App.OfflineStockDatas.alphaVantageOffline.historylist.IndexOf(data);

                RefreshDisplayData();
            }
            catch
            {

            }

        }

        /// <summary>
        /// Refresh data
        /// </summary>
        private void RefreshDisplayData()
        {
            if (dataGridSelectedIndex >= 0)
            {
                //frissíti a kijelzett historyt
                App.OfflineStockDataHistoryDisplay.Refresh((int)DisplayCountComboBox.SelectedItem, dataGridSelectedIndex);

                selectedHistoryChart.VerticalAxis.HorizontalAlignment = HorizontalAlignment.Center;

            }
        }

        /// <summary>
        /// Garph data show on cursor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChartTrackBallBehavior_TrackInfoUpdated(object sender, Telerik.UI.Xaml.Controls.Chart.TrackBallInfoEventArgs e)
        {
            try
            {
                AlphaVantageOffline.WebData DataItem = (e.Context.ClosestDataPoint.DataPoint.DataItem as AlphaVantageOffline.WebData);
                e.Context.ClosestDataPoint.DisplayHeader = DataItem.DateTime;
            }
            catch
            {
                //hiba
            }
        }

        /// <summary>
        /// Display combo box selected 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayCountComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DisplayCountComboBox.SelectedItem = (sender as ComboBox).SelectedItem;
            App.OfflineStockDataHistoryDisplay.DisplayCountComboBox = (int)DisplayCountComboBox.SelectedItem;
            RefreshDisplayData();
        }

        /// <summary>
        /// Display combo box selected 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OfflineTimerComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OfflineTimerComboBox.SelectedItem = (sender as ComboBox).SelectedItem;
            App.OfflineStockDatas.TimerOffline.Pause();
            App.OfflineStockDatas.TimerOffline = new TimerOffline(App.OfflineStockDatas.alphaVantageOffline, (int)OfflineTimerComboBox.SelectedItem);
            App.OfflineStockDataProgressBar.Pause();
        }

        /// <summary>
        /// Investment manager at double tap
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void AlphavantageTable_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            try
            {
                var data = ((sender as Telerik.UI.Xaml.Controls.Grid.RadDataGrid).SelectedItem) as System.Collections.ObjectModel.ObservableCollection<AlphaVantageOffline.WebData>;
                dataGridSelectedIndex = App.OfflineStockDatas.alphaVantageOffline.historylist.IndexOf(data);

                investmentData = new OfflineStockDataInvestment(data.ElementAt(0));

                Border border = new Border
                {
                    BorderBrush = new SolidColorBrush(Windows.UI.Colors.Gray),
                    BorderThickness = new Thickness() { Bottom = 2, Left = 2, Right = 2, Top = 2 }
                };

                StackPanel stackPanel = new StackPanel();

                //Info Box
                RichTextBlock info = new RichTextBlock();

                Run runInfo = new Run
                {
                    Text = investmentData.Info()
                };

                Paragraph paragraph = new Paragraph();
                paragraph.Inlines.Add(runInfo);

                info.Height = 200;
                info.Width = 600;
                info.Blocks.Add(paragraph);

                //Info Box2
                RichTextBlock info2 = new RichTextBlock();

                Run runInfo2 = new Run
                {
                    Text = investmentData.Info2()
                };

                Paragraph paragraph2 = new Paragraph();
                paragraph2.Inlines.Add(runInfo2);

                info2.Height = 200;
                info2.Width = 600;
                info2.Blocks.Add(paragraph2);

                //Info grid
                Grid InfoGrid = new Grid();
                ColumnDefinition col1Info = new ColumnDefinition();
                ColumnDefinition col2Info = new ColumnDefinition();
                ColumnDefinition col3Info = new ColumnDefinition();
                col1Info.Width = new GridLength(1, GridUnitType.Star);
                col2Info.Width = new GridLength(1, GridUnitType.Star);
                col3Info.Width = new GridLength(1, GridUnitType.Star);

                InfoGrid.ColumnDefinitions.Add(col1Info);
                InfoGrid.ColumnDefinitions.Add(col2Info);
                InfoGrid.ColumnDefinitions.Add(col3Info);

                InfoGrid.Children.Add(info);
                InfoGrid.Children.Add(info2);

                Grid.SetColumn(info, 0);
                Grid.SetColumn(info2, 1);
                Grid.SetColumnSpan(info2, 2);

                stackPanel.Children.Add(InfoGrid);

                //Invesmtment Change Grid
                TextBlock textBlockChangeString = new TextBlock
                {
                    Text = ResourceLoader.GetForViewIndependentUse().GetString("GeneralInvestedPrice") + ": "
            };

                TextBlock textBlockChange = new TextBlock();
                textBlockChange.SetBinding(TextBlock.TextProperty, new Binding() { Path = new PropertyPath("Change"), Source = investmentData, Mode = BindingMode.OneWay });

                Grid InvestmentChangeGrid = new Grid();
                ColumnDefinition col1I = new ColumnDefinition();
                ColumnDefinition col2I = new ColumnDefinition();
                ColumnDefinition col3I = new ColumnDefinition();
                col1I.Width = new GridLength(1, GridUnitType.Star);
                col2I.Width = new GridLength(1, GridUnitType.Star);
                col3I.Width = new GridLength(1, GridUnitType.Star);

                InvestmentChangeGrid.ColumnDefinitions.Add(col1I);
                InvestmentChangeGrid.ColumnDefinitions.Add(col2I);
                InvestmentChangeGrid.ColumnDefinitions.Add(col3I);

                InvestmentChangeGrid.Children.Add(textBlockChangeString);
                InvestmentChangeGrid.Children.Add(textBlockChange);

                Grid.SetColumn(textBlockChangeString, 0);
                Grid.SetColumn(textBlockChange, 1);
                Grid.SetColumnSpan(textBlockChange, 2);


                stackPanel.Children.Add(InvestmentChangeGrid);

                //Stocks amount grid
                TextBlock textBlockStocksString = new TextBlock
                {
                    Text = ResourceLoader.GetForViewIndependentUse().GetString("GeneralStocks") + ": "
                };

                TextBlock textBlockStocks = new TextBlock();
                textBlockStocks.SetBinding(TextBlock.TextProperty, new Binding() { Path = new PropertyPath("Stocks"), Source = investmentData, Mode = BindingMode.OneWay });

                Grid StockGrid = new Grid();
                ColumnDefinition col1S = new ColumnDefinition();
                ColumnDefinition col2S = new ColumnDefinition();
                ColumnDefinition col3S = new ColumnDefinition();
                col1S.Width = new GridLength(1, GridUnitType.Star);
                col2S.Width = new GridLength(1, GridUnitType.Star);
                col3S.Width = new GridLength(1, GridUnitType.Star);

                StockGrid.ColumnDefinitions.Add(col1S);
                StockGrid.ColumnDefinitions.Add(col2S);
                StockGrid.ColumnDefinitions.Add(col3S);

                StockGrid.Children.Add(textBlockStocksString);
                StockGrid.Children.Add(textBlockStocks);

                Grid.SetColumn(textBlockStocksString, 0);
                Grid.SetColumn(textBlockStocks, 1);
                Grid.SetColumnSpan(textBlockStocks, 2);

                stackPanel.Children.Add(StockGrid);

                //Add remove buttons
                Button addButton = new Button
                {
                    Content = ResourceLoader.GetForViewIndependentUse().GetString("GeneralAdd")
                };
                addButton.Click += AddButton_Click;
                addButton.HorizontalAlignment = HorizontalAlignment.Stretch;

                Button removeButton = new Button
                {
                    Content = ResourceLoader.GetForViewIndependentUse().GetString("GeneralRemove")
                };
                removeButton.Click += RemoveButton_Click;
                removeButton.HorizontalAlignment = HorizontalAlignment.Stretch;

                Grid buttonsGrid = new Grid();
                ColumnDefinition col1 = new ColumnDefinition();
                ColumnDefinition col2 = new ColumnDefinition();
                col1.Width = new GridLength(1, GridUnitType.Star);
                col2.Width = new GridLength(1, GridUnitType.Star);

                buttonsGrid.ColumnDefinitions.Add(col1);
                buttonsGrid.ColumnDefinitions.Add(col2);

                buttonsGrid.Children.Add(addButton);
                buttonsGrid.Children.Add(removeButton);

                Grid.SetColumn(addButton, 0);
                Grid.SetColumn(removeButton, 1);

                stackPanel.Children.Add(buttonsGrid);

                border.Child = stackPanel;

                InvestmentDialogStackPanel.Children.Add(border);

                ContentDialogResult result = await InvestmentDialog.ShowAsync();

                if (result == ContentDialogResult.Primary)
                {
                    //SaveAsync
                    investmentData.DateTime = DateTime.Now;
                    App.OfflineStockDatas.alphaVantageOffline.historylist.ElementAt(dataGridSelectedIndex).ElementAt(0).InvestedStocks = investmentData.Stocks;
                    App.Player.PlayerAmountOffline -= investmentData.Change;
                    App.Player.NewInvestment(new OfflineStockDataInvestment(investmentData));
                    InvestmentDialogStackPanel.Children.Clear();
                    App.OfflineStockDatas.alphaVantageOffline.SaveAsync();
                }
                else if (result == ContentDialogResult.Secondary)
                {
                    //Secondary
                }
                else
                {
                    //Close button
                    InvestmentDialogStackPanel.Children.Clear();

                }

            }
            catch
            {

            }

        }

        /// <summary>
        /// Add button, add stocks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            var stateShift = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Shift);
            bool shift = (stateShift & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;

            var stateControl = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Control);
            bool control = (stateControl & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;
            if (shift && control)
            {
                //egyszerre
            }
            else if (shift)
            {
                investmentData.IncreaseStocks(10);

            }
            else if (control)
            {
                investmentData.IncreaseStocks(50);
            }
            else
            {
                investmentData.IncreaseStocks(1);
            }
        }

        /// <summary>
        /// Remove button, decrease stock
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            var stateShift = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Shift);
            bool shift = (stateShift & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;

            var stateControl = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Control);
            bool control = (stateControl & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;
            if (shift && control)
            {
                //egyszerre
            }
            else if (shift)
            {
                investmentData.DecreaseStocks(10);

            }
            else if (control)
            {
                investmentData.DecreaseStocks(50);
            }
            else
            {
                investmentData.DecreaseStocks(1);
            }
        }

        /// <summary>
        /// Database control restart
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonRestart_Click(object sender, RoutedEventArgs e)
        {
            //reset time
            App.OfflineStockDatas.SimulatedTime.SimulatedTime = new DateTime(2019, 04, 1, 9, 31, 0);
            App.OfflineStockDatas.SimulatedTime.SaveAsync();

            //reset remaining and current data and display history
            App.OfflineStockDatas.alphaVantageOffline.allData = new System.Collections.ObjectModel.ObservableCollection<System.Collections.ObjectModel.ObservableCollection<AlphaVantageOffline.WebData>>();
            App.OfflineStockDatas.alphaVantageOffline.historylist = new System.Collections.ObjectModel.ObservableCollection<System.Collections.ObjectModel.ObservableCollection<AlphaVantageOffline.WebData>>();
            App.OfflineStockDataHistoryDisplay.displayList = new System.Collections.ObjectModel.ObservableCollection<AlphaVantageOffline.WebData>();

            //load reamining data
            App.OfflineStockDatas.alphaVantageOffline.LoadRESW();

            //save
            App.OfflineStockDatas.alphaVantageOffline.SaveAsync();

            //refresh page
            App.mainPageReference.contentFrame.Navigate(typeof(OfflineStockDataPage));

            App.OfflineStockDataProgressBar.Value = 0;
        }

        /// <summary>
        /// Database control play
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPlay_Click(object sender, RoutedEventArgs e)
        {
            App.OfflineStockDatas.TimerOffline.Start();
            App.OfflineStockDataProgressBar.Start();
        }

        /// <summary>
        /// Database control pause
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPause_Click(object sender, RoutedEventArgs e)
        {
            App.OfflineStockDatas.TimerOffline.Pause();
            App.OfflineStockDataProgressBar.Pause();
        }
    }
}
