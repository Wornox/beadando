﻿using System;
using System.ComponentModel;
using Windows.ApplicationModel.Resources;

namespace StockExchangeSimulation
{
    public class OfflineStockDataInvestment : INotifyPropertyChanged
    {
        public OfflineStockDataInvestment()
        {

        }

        public OfflineStockDataInvestment(AlphaVantageOffline.WebData webdata)
        {
            _stocks = webdata.InvestedStocks;
            _startStocks = webdata.InvestedStocks;

            Name = webdata.Name;
            Symbol = webdata.Symbol;
            High = webdata.High;
            Low = webdata.Low;
            Open = webdata.Open;
            Close = webdata.Close;
            DateTime = webdata.DateTime;
        }

        public OfflineStockDataInvestment(OfflineStockDataInvestment investment)
        {
            Name = investment.Name;
            Symbol = investment.Symbol;
            High = investment.High;
            Low = investment.Low;
            Open = investment.Open;
            Close = investment.Close;
            DateTime = investment.DateTime;
            Stocks = investment.Stocks;
            Change = investment.Change;
            StocksChange = investment.StocksChange;
        }

        public void CopyProperties(OfflineStockDataInvestment investment)
        {
            Name = investment.Name;
            Symbol = investment.Symbol;
            High = investment.High;
            Low = investment.Low;
            Open = investment.Open;
            Close = investment.Close;
            DateTime = investment.DateTime;
            Stocks = investment.Stocks;
            Change = investment.Change;
            StocksChange = investment.StocksChange;

        }

        public string Name { get; set; }
        public string Symbol { get; set; }

        public DateTime DateTime { get; set; }

        private int _startStocks;

        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }

        private int _stocks;
        public int Stocks
        {
            get { return _stocks; }
            set
            {
                this._stocks = value;
                RaisePropertyChanged("Stocks");
            }
        }

        private int _stocksChange;
        public int StocksChange
        {
            get { return _stocksChange; }
            set
            {
                this._stocksChange = value;
                RaisePropertyChanged("StocksChange");
            }
        }

        private double _change;
        public double Change
        {
            get { return _change; }
            set
            {
                this._change = value;
                RaisePropertyChanged("Change");
            }

        }

        /// <summary>
        /// Investrment manager incvrese stock
        /// </summary>
        /// <param name="number"></param>
        public void IncreaseStocks(int number)
        {
            int newStocks = _stocks + number;
            double newChange = _change + (High + Low)/2 * number;
            newChange = Math.Round(newChange, 5);
            if (App.Player.PlayerAmountOffline > newChange || _startStocks > newStocks)
            {
                Stocks = newStocks;
                Change = newChange;
                StocksChange = _stocks - _startStocks;
            }
        }

        /// <summary>
        /// Investment manager decrease stock
        /// </summary>
        /// <param name="number"></param>
        public void DecreaseStocks(int number)
        {
            int newStocks = _stocks - number;
            double newChange = _change - (High + Low) / 2 * number;
            newChange = Math.Round(newChange, 5);
            if (newStocks >= 0)
            {
                Stocks = newStocks;
                Change = newChange;
                if (Change < 1 && Change > -1) Change = 0;
                StocksChange = _stocks - _startStocks;
            }
        }

        /// <summary>
        /// investment data show
        /// </summary>
        /// <returns></returns>
        public string Info()
        {
            return
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageName") + "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageSymbol") + "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageHigh") + "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageLow") + "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageOpen") + "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageClose") + "\n" +
                ResourceLoader.GetForViewIndependentUse().GetString("DataGridColumnWebStockDataAlphaVantageDateTime");
        }

        /// <summary>
        /// company data show
        /// </summary>
        /// <returns></returns>
        public string Info2()
        {
            return
                 Name + "\n" +
                 Symbol + "\n" +
                 High + "\n" +
                 Low + "\n" +
                 Open + "\n" +
                 Close + "\n" +
                 DateTime;
        }

        #region PropertyChangedEventHandler

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion
    }
}
