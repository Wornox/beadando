﻿using Microsoft.Toolkit.Uwp.Helpers;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace StockExchangeSimulation
{
    public class OfflineStockData
    {
        public AlphaVantageOffline alphaVantageOffline = new AlphaVantageOffline();

        public DateTimeNotify SimulatedTime = new DateTimeNotify();

        public TimerOffline TimerOffline;

        /// <summary>
        /// Constructor
        /// </summary>
        public OfflineStockData()
        {
            TimerOffline = new TimerOffline(alphaVantageOffline, 1);

        }

    }

    public class DateTimeNotify : INotifyPropertyChanged
    {
        LocalObjectStorageHelper SaveLoadhelper = new LocalObjectStorageHelper();
        readonly string SaveLoadhelperKey = "offlineStockDataSimulatedTime";

        private DateTime simulatedTime;
        public DateTime SimulatedTime
        {
            get { return simulatedTime; }
            set
            {
                this.simulatedTime = value;
                RaisePropertyChanged(nameof(SimulatedTime));
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public DateTimeNotify()
        {
            LoadAsync();
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {
            try
            {
                await SaveLoadhelper.SaveFileAsync(SaveLoadhelperKey, simulatedTime);
            }
            catch
            {
                //ha éppen foglalt a file, akkor excaptiont dob
            }
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <returns></returns>
        public async Task LoadAsync()
        {
            if (await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKey))
            {
                DateTime temp = await SaveLoadhelper.ReadFileAsync<DateTime>(SaveLoadhelperKey);
                if (temp != null) simulatedTime = temp;
            } else
            {
                //ha még nem volt lementve
                SimulatedTime = new DateTime(2019, 04, 1, 9, 31, 0);
            }
        }

        #region PropertyChangedEventHandler

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

    }

    public class TimerOffline
    {
        DispatcherTimer dispatcherTimer;
        int refreshtime;
        AlphaVantageOffline alphaVantageOffline;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="alphaVantageOffline"></param>
        /// <param name="refreshtime"></param>
        public TimerOffline(AlphaVantageOffline alphaVantageOffline, int refreshtime)
        {
            this.alphaVantageOffline = alphaVantageOffline;
            this.refreshtime = refreshtime;
            DispatcherTimerSetup();
            App.OfflineStockDataProgressBar.Range = refreshtime;
        }

        /// <summary>
        /// Timer setup
        /// </summary>
        public void DispatcherTimerSetup()
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, refreshtime);

        }

        /// <summary>
        /// Timer tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void dispatcherTimer_Tick(object sender, object e)
        {
            App.OfflineStockDatas.SimulatedTime.SimulatedTime = App.OfflineStockDatas.SimulatedTime.SimulatedTime.AddMinutes(1);

            //Nap végén előre ugrik a következő nyitási időpontra
            if(App.OfflineStockDatas.SimulatedTime.SimulatedTime.Hour >= 16)
            {
                App.OfflineStockDatas.SimulatedTime.SimulatedTime = App.OfflineStockDatas.SimulatedTime.SimulatedTime.AddHours(17);
                App.OfflineStockDatas.SimulatedTime.SimulatedTime = App.OfflineStockDatas.SimulatedTime.SimulatedTime.AddMinutes(30);
            }

            App.OfflineStockDatas.SimulatedTime.SaveAsync();

            alphaVantageOffline.RefreshDataAsync();

            App.OfflineStockDatas.alphaVantageOffline.SaveAsync();
        }

        /// <summary>
        /// Start timer
        /// </summary>
        public void Start()
        {
            dispatcherTimer.Start();
        }

        /// <summary>
        /// Pause timer
        /// </summary>
        public void Pause()
        {
            dispatcherTimer.Stop();
        }

    }

    public class AlphaVantageOffline
    {
        /// <summary>
        /// Company data representation
        /// </summary>
        public class WebData : INotifyPropertyChanged
        {
            public string Name { get; set; }
            public string Symbol { get; set; }

            public string Type { get; set; }
            public string Region { get; set; }
            public string MarketOpen { get; set; }
            public string MarketClose { get; set; }
            public string TimeZone { get; set; }
            public string Currency { get; set; }

            public double Open { get; set; }
            public double High { get; set; }
            public double Low { get; set; }
            public double Close { get; set; }
            public int Volume { get; set; }
            public DateTime DateTime { get; set; }


            private int investedStocks;
            public int InvestedStocks
            {
                get { return investedStocks; }
                set
                {
                    this.investedStocks = value;
                    RaisePropertyChanged("InvestedStocks");
                }
            }

            private bool refresh;
            public bool Refresh
            {
                get { return refresh; }
                set
                {
                    this.refresh = value;
                    RaisePropertyChanged("Refresh");
                }
            }

            /// <summary>
            /// Constructor
            /// </summary>
            public WebData()
            {

            }

            /// <summary>
            /// Copy constructor
            /// </summary>
            /// <param name="other"></param>
            public WebData(WebData other)
            {
                Name = other.Name;
                Symbol = other.Symbol;
                Type = other.Type;
                Region = other.Region;
                MarketOpen = other.MarketOpen;
                MarketClose = other.MarketClose;
                TimeZone = other.TimeZone;
                Currency = other.Currency;
                Open = other.Open;
                High = other.High;
                Low = other.Low;
                Close = other.Close;
                Volume = other.Volume;
                DateTime = other.DateTime;
                InvestedStocks = other.InvestedStocks;
                Refresh = other.Refresh;
            }

            /// <summary>
            /// Tostring()
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return Name + " " + Symbol + " " + Region;
            }

            #region PropertyChangedEventHandler

            public event PropertyChangedEventHandler PropertyChanged;

            protected void RaisePropertyChanged(string name)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(name));
                }
            }

            #endregion

        }

        public ObservableCollection<ObservableCollection<WebData>> allData { get; set; }

        public ObservableCollection<ObservableCollection<WebData>> historylist { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public AlphaVantageOffline()
        {
            historylist = new ObservableCollection<ObservableCollection<WebData>>();
            LoadAsync();

        }

        ///<summary> 
        ///This is called from timer
        ///</summary>
        async public void RefreshDataAsync()
        {
            int index = 0;
            foreach(var company in allData)
            {
                if(company.Count != 0 && company.ElementAt(0).DateTime <= App.OfflineStockDatas.SimulatedTime.SimulatedTime)
                {
                    //létrehozok egy új listát a régi adatokból
                    ObservableCollection<WebData> newList = new ObservableCollection<WebData>();
                    foreach(var element in historylist.ElementAt(index))
                    {
                        newList.Insert(historylist.ElementAt(index).IndexOf(element), element);
                    }

                    //az új listába belerakom az új adaatot
                    newList.Insert(0, company.ElementAt(0));

                    //törlöm a allData-ból az adatot
                    allData.ElementAt(index).RemoveAt(0);

                    //lementem az invested stockokat
                    int InvestedStocks = historylist.ElementAt(index).ElementAt(0).InvestedStocks;

                    //törlöm az adatbázsiból a régi listát
                    historylist.RemoveAt(index);

                    //belerakom az új listát
                    historylist.Insert(index, newList);

                    //visszatöltöm az investedstockokat
                    historylist.ElementAt(index).ElementAt(0).InvestedStocks = InvestedStocks;
                }
                index++;
            }

            //refresh selected data history list (left buttom grid)
            //frissíti a kijelzett historyt
            App.OfflineStockDataHistoryDisplay.Refresh();
        }

        LocalObjectStorageHelper SaveLoadhelper = new LocalObjectStorageHelper();
        string SaveLoadhelperKey = "offlineStockData";

        string SaveLoadhelperKeyAllData = "offlineStockDataAllData";

        /// <summary>
        /// Load
        /// </summary>
        /// <returns></returns>
        public async Task LoadAsync()
        {
            if (await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKey) && await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyAllData))
            {
                //ha már létezett a file akkor azt tölti be
                ObservableCollection<ObservableCollection<AlphaVantageOffline.WebData>> tempHistoryList = await SaveLoadhelper.ReadFileAsync<ObservableCollection<ObservableCollection<AlphaVantageOffline.WebData>>>(SaveLoadhelperKey);
                ObservableCollection<ObservableCollection<AlphaVantageOffline.WebData>> tempAllData = await SaveLoadhelper.ReadFileAsync<ObservableCollection<ObservableCollection<AlphaVantageOffline.WebData>>>(SaveLoadhelperKeyAllData);
                if (tempHistoryList != null) historylist = tempHistoryList;
                if (tempAllData != null) allData = tempAllData;
            }
            else
            {
                //ha nem akkor a resw fileból
                LoadRESW();
            }
            
        }

        ///<summary>
        ///Load all saved company data from resw file
        ///</summary>///
        public void LoadRESW()
        {
            var asd = Windows.ApplicationModel.Resources.Core.ResourceManager.Current.MainResourceMap.GetSubtree("OfflineStockData");
            var datastring = asd.GetValue("data").ValueAsString;

            allData = Newtonsoft.Json.JsonConvert.DeserializeObject<ObservableCollection<ObservableCollection<WebData>>>(datastring);

            //megforditja a sorrendet
            for (int i = 0; i < allData.Count; ++i)
            {
                ObservableCollection<WebData> newlistAtZero = new ObservableCollection<WebData>(allData.ElementAt(i).Reverse());
                allData.RemoveAt(i);
                allData.Insert(i, newlistAtZero);
            }

            //az első elemet hozzáadja a historylisthez, ehhez egy új listát fűz hozzá ami tartlamazzaza az 1 adatot
            foreach (var company in allData)
            {
                ObservableCollection<WebData> newlist = new ObservableCollection<WebData>();
                newlist.Add(company.ElementAt(0));
                company.RemoveAt(0);
                historylist.Add(newlist);
            }

            SaveAsync();
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {
            try
            {
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKey, historylist);
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyAllData, allData);
            }
            catch
            {
                //ha éppen foglalt a file, akkor excaptiont dob
            }
        }

    }
}
