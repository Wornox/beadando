﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace StockExchangeSimulation
{
    public class LocalConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var alldata = (System.Collections.ObjectModel.ObservableCollection<LocalStockData>)value;
            var displaydata = new System.Collections.ObjectModel.ObservableCollection<LocalStockData>();

            foreach (var element in alldata)
            {
               
                //keres
                bool found = false;
                int foundindex = -1;
                foreach (var displayelement in displaydata)
                {
                    int index = 0;
                    if (element.CompanyName == displayelement.CompanyName)
                    {
                        found = true;
                        foundindex = index;
                    }
                    index++;
                }

                //kicserléi ha van új
                if (foundindex > -1 && found)
                {
                    displaydata.RemoveAt(foundindex);
                    displaydata.Add(element);
                }

                //ha nincs benne, akkor simán hozzáadja
                if (!found)
                {
                    displaydata.Add(element);
                }
            }

            //ha 0 a befektett részvény, azt nem mutatja a grafikonokon
            ObservableCollection<LocalStockData> lsdList = new ObservableCollection<LocalStockData>();

            foreach (var element in displaydata)
            {
                if (element.OwnedStocks == 0)
                {
                    lsdList.Add(element);
                    break;
                }
            }

            foreach(var element in lsdList)
            {
                displaydata.Remove(element);
            }

            
            return displaydata;

        }

        // ConvertBack is not implemented for a OneWay binding.
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class OnlyInvestedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            switch ((string)parameter)
            {
                case "offline":
                    var alldata = (System.Collections.ObjectModel.ObservableCollection<System.Collections.ObjectModel.ObservableCollection<AlphaVantageOffline.WebData>>)value;
                    var displaydata = new System.Collections.ObjectModel.ObservableCollection<AlphaVantageOffline.WebData>();
                    foreach (var companylist in alldata)
                    {
                        foreach(var investedcompany in App.Player.offlineStockDataInvestmentList)
                        {
                            if (companylist.ElementAt(0).Name == investedcompany.Name) displaydata.Add(companylist.ElementAt(0));
                        }
                    }
                    return displaydata;

                case "online":
                    var alldataOnline = (System.Collections.ObjectModel.ObservableCollection<System.Collections.ObjectModel.ObservableCollection<AlphaVantage.WebData>>)value;
                    var displaydataOnline = new System.Collections.ObjectModel.ObservableCollection<AlphaVantage.WebData>();
                    foreach (var companylist in alldataOnline)
                    {
                        foreach (var investedcompany in App.Player.webStockDataInvestmentList)
                        {
                            if (companylist.ElementAt(0).Name == investedcompany.Name) displaydataOnline.Add(companylist.ElementAt(0));
                        }
                    }
                    return displaydataOnline;
                
            }
            return null;

        }

        // ConvertBack is not implemented for a OneWay binding.
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class CurrentAndInvestedOfflinePriceColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var data = (OfflineStockDataInvestment)value;

            var historylist = App.OfflineStockDatas.alphaVantageOffline.historylist;

            double freshprice = 0;

            var color = new SolidColorBrush(Windows.UI.Colors.Gray);

            switch ((string)parameter)
            {
                case "open":
                    foreach(var element in historylist) { if(element.ElementAt(0).Name == data.Name) freshprice = element.ElementAt(0).Open; }
                    if (data.Open > freshprice) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                    else if (data.Open < freshprice) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                    else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                    break;
                case "high":
                    foreach (var element in historylist) { if (element.ElementAt(0).Name == data.Name) freshprice = element.ElementAt(0).High; }
                    if (data.High > freshprice) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                    else if (data.High < freshprice) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                    else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                    break;
                case "low":
                    foreach (var element in historylist) { if (element.ElementAt(0).Name == data.Name) freshprice = element.ElementAt(0).Low; }
                    if (data.Low > freshprice) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                    else if (data.Low < freshprice) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                    else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                    break;
                case "close":
                    foreach (var element in historylist) { if (element.ElementAt(0).Name == data.Name) freshprice = element.ElementAt(0).Close; }
                    if (data.Close > freshprice) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                    else if (data.Close < freshprice) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                    else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                    break;
            }
            return color;
        }

        // ConvertBack is not implemented for a OneWay binding.
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class CurrentAndInvestedPriceOnlineColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var data = (OnlineStockDataInvestment)value;

            var historylist = App.WebStockDatas.alphaVantage.historylist;

            double freshprice = 0;

            var color = new SolidColorBrush(Windows.UI.Colors.Gray);

            switch ((string)parameter)
            {
                case "open":
                    foreach (var element in historylist) { if (element.ElementAt(0).Name == data.Name) freshprice = element.ElementAt(0).Open; }
                    if (data.Open > freshprice) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                    else if (data.Open < freshprice) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                    else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                    break;
                case "high":
                    foreach (var element in historylist) { if (element.ElementAt(0).Name == data.Name) freshprice = element.ElementAt(0).High; }
                    if (data.High > freshprice) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                    else if (data.High < freshprice) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                    else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                    break;
                case "low":
                    foreach (var element in historylist) { if (element.ElementAt(0).Name == data.Name) freshprice = element.ElementAt(0).Low; }
                    if (data.Low > freshprice) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                    else if (data.Low < freshprice) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                    else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                    break;
                case "close":
                    foreach (var element in historylist) { if (element.ElementAt(0).Name == data.Name) freshprice = element.ElementAt(0).Close; }
                    if (data.Close > freshprice) color = new SolidColorBrush(Windows.UI.Colors.IndianRed);
                    else if (data.Close < freshprice) color = new SolidColorBrush(Windows.UI.Colors.DeepSkyBlue);
                    else color = new SolidColorBrush(Windows.UI.Colors.Gray);
                    break;
            }
            return color;
        }

        // ConvertBack is not implemented for a OneWay binding.
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class OnlineConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var alldata = (ObservableCollection<OnlineStockDataInvestment>)value;
            var displaydata = new ObservableCollection<OnlineStockDataInvestment>();

            foreach(var element in alldata)
            {
                displaydata.Add(element);
            }

            //ha 0 a befektett részvény, azt nem mutatja a grafikonokon
            ObservableCollection<OnlineStockDataInvestment> lsdList = new ObservableCollection<OnlineStockDataInvestment>();

            foreach (var element in displaydata)
            {
                if (element.Stocks == 0)
                {
                    lsdList.Add(element);
                    break;
                }
            }

            foreach (var element in lsdList)
            {
                displaydata.Remove(element);
            }


            return displaydata;

        }

        // ConvertBack is not implemented for a OneWay binding.
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class OfflineConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var alldata = (ObservableCollection<OfflineStockDataInvestment>)value;
            var displaydata = new ObservableCollection<OfflineStockDataInvestment>();

            foreach (var element in alldata)
            {
                displaydata.Add(element);
            }

            //ha 0 a befektett részvény, azt nem mutatja a grafikonokon
            ObservableCollection<OfflineStockDataInvestment> lsdList = new ObservableCollection<OfflineStockDataInvestment>();

            foreach (var element in displaydata)
            {
                if (element.Stocks == 0)
                {
                    lsdList.Add(element);
                    break;
                }
            }

            foreach (var element in lsdList)
            {
                displaydata.Remove(element);
            }


            return displaydata;

        }

        // ConvertBack is not implemented for a OneWay binding.
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PlayerPage : Page
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public PlayerPage()
        {
            this.InitializeComponent();

            //Pivot
            PivotPlayerLocalDatabase.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerLocalDatabase));
            PivotPlayerOnlineDatabase.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerOnlineDatabase));
            PivotPlayerOfflineDatabase.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerOfflineDatabase));



            PivotPlayerHistoryInvestmentLocal.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerHistoryInvestmentLocal));

            PivotPlayerCurrentInvestmentOffline.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerCurrentInvestmentOffline));
            PivotPlayerHistoryInvestmentOffline.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerHistoryInvestmentOffline));

            PivotPlayerCurrentInvestmentOnline.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerCurrentInvestmentOnline));
            PivotPlayerHistoryInvestmentOnline.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerHistoryInvestmentOnline));



            PivotPlayerStocksLocal.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerStocksLocal));

            PivotPlayerStocksOffline.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerStocksOffline));
            PivotPlayerInvestmentOffline.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerInvestmentOffline));

            PivotPlayerStocksOnline.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerStocksOnline));
            PivotPlayerInvestmentOnline.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(PivotPlayerInvestmentOnline));


            //Local
            DataGridPlayerInvestmentLocalListName.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentLocalListName));
            DataGridPlayerInvestmentLocalListDescription.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentLocalListDescription));
            DataGridPlayerInvestmentLocalListCurrentValue.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentLocalListCurrentValue));
            DataGridPlayerInvestmentLocalListCurrentReputation.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentLocalListCurrentReputation));
            DataGridPlayerInvestmentLocalListOwnedStocks.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentLocalListOwnedStocks));
            DataGridPlayerInvestmentLocalListInvestment.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentLocalListInvestment));
            DataGridPlayerInvestmentLocalListDate.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentLocalListDate));

            //Online - Current Investment - History
            DataGridPlayerInvestmentListName.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentListName));
            DataGridPlayerInvestmentListStocks.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentListStocks));
            DataGridPlayerInvestmentListChange.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentListChange));
            DataGridPlayerInvestmentListOpen.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentListOpen));
            DataGridPlayerInvestmentListHigh.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentListHigh));
            DataGridPlayerInvestmentListLow.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentListLow));
            DataGridPlayerInvestmentListClose.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentListClose));
            DataGridPlayerInvestmentListDateTime.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentListDateTime));

            //Online - Current Investment - History all
            DataGridPlayerInvestmentHistoryName.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentHistoryName));
            DataGridPlayerInvestmentHistoryStocksChange.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentHistoryStocksChange));
            DataGridPlayerInvestmentHistoryChange.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentHistoryChange));
            DataGridPlayerInvestmentHistoryOpen.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentHistoryOpen));
            DataGridPlayerInvestmentHistoryHigh.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentHistoryHigh));
            DataGridPlayerInvestmentHistoryLow.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentHistoryLow));
            DataGridPlayerInvestmentHistoryClose.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentHistoryClose));
            DataGridPlayerInvestmentHistoryDateTime.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentHistoryDateTime));

            //Online - Current Investment - Current / fresh data
            DataGridPlayerInvestmentCurrentName.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentCurrentName));
            DataGridPlayerInvestmentCurrentOpen.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentCurrentOpen));
            DataGridPlayerInvestmentCurrentHigh.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentCurrentHigh));
            DataGridPlayerInvestmentCurrentLow.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentCurrentLow));
            DataGridPlayerInvestmentCurrentClose.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentCurrentClose));
            DataGridPlayerInvestmentCurrentCurrentDateTime.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerInvestmentCurrentCurrentDateTime));


            //Offline - Current Investment - History
            DataGridPlayerOfflineInvestmentListName.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentListName));
            DataGridPlayerOfflineInvestmentListStocks.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentListStocks));
            DataGridPlayerOfflineInvestmentListChange.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentListChange));
            DataGridPlayerOfflineInvestmentListOpen.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentListOpen));
            DataGridPlayerOfflineInvestmentListHigh.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentListHigh));
            DataGridPlayerOfflineInvestmentListLow.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentListLow));
            DataGridPlayerOfflineInvestmentListClose.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentListClose));
            DataGridPlayerOfflineInvestmentListDateTime.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentListDateTime));

            //Offline - History Investment - History all
            DataGridPlayerOfflineInvestmentHistoryName.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentHistoryName));
            DataGridPlayerOfflineInvestmentHistoryStocksChange.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentHistoryStocksChange));
            DataGridPlayerOfflineInvestmentHistoryChange.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentHistoryChange));
            DataGridPlayerOfflineInvestmentHistoryOpen.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentHistoryOpen));
            DataGridPlayerOfflineInvestmentHistoryHigh.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentHistoryHigh));
            DataGridPlayerOfflineInvestmentHistoryLow.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentHistoryLow));
            DataGridPlayerOfflineInvestmentHistoryClose.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentHistoryClose));
            DataGridPlayerOfflineInvestmentHistoryDateTime.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentHistoryDateTime));

            //offline - Current Investment - Current / fresh data
            DataGridPlayerOfflineInvestmentCurrentName.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentCurrentName));
            DataGridPlayerOfflineInvestmentCurrentOpen.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentCurrentOpen));
            DataGridPlayerOfflineInvestmentCurrentHigh.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentCurrentHigh));
            DataGridPlayerOfflineInvestmentCurrentLow.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentCurrentLow));
            DataGridPlayerOfflineInvestmentCurrentClose.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentCurrentClose));
            DataGridPlayerOfflineInvestmentCurrentCurrentDateTime.Header = ResourceLoader.GetForViewIndependentUse().GetString(nameof(DataGridPlayerOfflineInvestmentCurrentCurrentDateTime));

        }
    }
}
