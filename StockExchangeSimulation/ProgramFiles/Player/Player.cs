﻿using Microsoft.Toolkit.Uwp.Helpers;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace StockExchangeSimulation
{
    public class Player : INotifyPropertyChanged
    {
        public Player()
        {
            webStockDataInvestmentList  = new ObservableCollection<OnlineStockDataInvestment>();
            webStockDataInvestmentHistoryList  = new ObservableCollection<OnlineStockDataInvestment>();

            offlineStockDataInvestmentList  = new ObservableCollection<OfflineStockDataInvestment>();
            offlineStockDataInvestmentHistoryList  = new ObservableCollection<OfflineStockDataInvestment>();

            LoadAsync();

        }

        #region properties
        public ObservableCollection<OnlineStockDataInvestment> webStockDataInvestmentList { get; set; } 
        public ObservableCollection<OnlineStockDataInvestment> webStockDataInvestmentHistoryList { get; set; }

        public ObservableCollection<OfflineStockDataInvestment> offlineStockDataInvestmentList { get; set; } 
        public ObservableCollection<OfflineStockDataInvestment> offlineStockDataInvestmentHistoryList { get; set; } 

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                this._Name = value;
                RaisePropertyChanged(nameof(Name));
            }
        }

        private double _PlayerAmountLocal;
        public double PlayerAmountLocal
        {
            get { return _PlayerAmountLocal; }
            set
            {
                this._PlayerAmountLocal = value;
                RaisePropertyChanged(nameof(PlayerAmountLocal));
            }
        }

        private double _PlayerAmountOnline;
        public double PlayerAmountOnline
        {
            get { return _PlayerAmountOnline; }
            set
            {
                this._PlayerAmountOnline = value;
                RaisePropertyChanged(nameof(PlayerAmountOnline));
            }
        }

        private double _PlayerAmountOffline;
        public double PlayerAmountOffline
        {
            get { return _PlayerAmountOffline; }
            set
            {
                this._PlayerAmountOffline = value;
                RaisePropertyChanged(nameof(PlayerAmountOffline));
            }
        }

        private string _AlphaVantageAPIKey;
        public string AlphaVantageAPIKey
        {
            get { return _AlphaVantageAPIKey; }
            set
            {
                this._AlphaVantageAPIKey = value;
                RaisePropertyChanged(nameof(AlphaVantageAPIKey));
            }
        }

        public void NewInvestment(OnlineStockDataInvestment investment)
        {
            webStockDataInvestmentHistoryList.Add(new OnlineStockDataInvestment(investment));

            bool createNew = true;
            int foundID = -1;
            int searchID = 0;
            foreach(var element in webStockDataInvestmentList)
            {
                if (element.Symbol == investment.Symbol)
                {
                    createNew = false;
                    foundID = searchID;
                }
                searchID++;
            }
            if (createNew)
            {
                webStockDataInvestmentList.Add(new OnlineStockDataInvestment(investment));
            } else
            {
                var stocks = webStockDataInvestmentList.ElementAt(foundID).Stocks;
                var stocksChange = webStockDataInvestmentList.ElementAt(foundID).StocksChange;
                var change = webStockDataInvestmentList.ElementAt(foundID).Change;

                webStockDataInvestmentList.ElementAt(foundID).CopyProperties(investment);

                webStockDataInvestmentList.ElementAt(foundID).StocksChange += stocksChange;
                webStockDataInvestmentList.ElementAt(foundID).Change += change;
            }
            SaveAsync();
        }

        public void NewInvestment(OfflineStockDataInvestment investment)
        {
            offlineStockDataInvestmentHistoryList.Add(new OfflineStockDataInvestment(investment));

            bool createNew = true;
            int foundID = -1;
            int searchID = 0;
            foreach (var element in offlineStockDataInvestmentList)
            {
                if (element.Symbol == investment.Symbol)
                {
                    createNew = false;
                    foundID = searchID;
                }
                searchID++;
            }
            if (createNew)
            {
                offlineStockDataInvestmentList.Add(new OfflineStockDataInvestment(investment));
            }
            else
            {
                var stocks = offlineStockDataInvestmentList.ElementAt(foundID).Stocks;
                var stocksChange = offlineStockDataInvestmentList.ElementAt(foundID).StocksChange;
                var change = offlineStockDataInvestmentList.ElementAt(foundID).Change;

                offlineStockDataInvestmentList.ElementAt(foundID).CopyProperties(investment);

                offlineStockDataInvestmentList.ElementAt(foundID).StocksChange += stocksChange;
                offlineStockDataInvestmentList.ElementAt(foundID).Change += change;
            }
            SaveAsync();
        }

        LocalObjectStorageHelper SaveLoadhelper = new LocalObjectStorageHelper();
        readonly string SaveLoadhelperKeyOffline = "PlayerOffline";
        readonly string SaveLoadhelperKeyOfflineHistory = "PlayerOfflineHistory";

        readonly string SaveLoadhelperKeyOnline = "PlayerOnline";
        readonly string SaveLoadhelperKeyOnlineHistory = "PlayerOnlineHistory";

        readonly string SaveLoadhelperKeyName = "PlayerName";
        readonly string SaveLoadhelperKeyPlayerAmountLocal = "PlayerAmountLocal";
        readonly string SaveLoadhelperKeyPlayerAmountOnline = "PlayerAmountOffline";
        readonly string SaveLoadhelperKeyPlayerAmountOffline = "PlayerAmountOnline";
        readonly string SaveLoadhelperKeyAlphaVantageAPIKey = "AlphaVantageAPIKey";

        #endregion

        /// <summary>
        /// Load
        /// </summary>
        /// <returns></returns>
        public async Task LoadAsync()
        {
            if (await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyName) && await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyPlayerAmountLocal) && await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyPlayerAmountOnline) && await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyPlayerAmountOffline))
            {
                //ha már létezett a file akkor azt tölti be
                string tempName = await SaveLoadhelper.ReadFileAsync<string>(SaveLoadhelperKeyName);
                double tempAmountLocal = await SaveLoadhelper.ReadFileAsync<double>(SaveLoadhelperKeyPlayerAmountLocal);
                double tempAmountOnline = await SaveLoadhelper.ReadFileAsync<double>(SaveLoadhelperKeyPlayerAmountOnline);
                double tempAmountOffline = await SaveLoadhelper.ReadFileAsync<double>(SaveLoadhelperKeyPlayerAmountOffline);
                Name = tempName;
                PlayerAmountLocal = tempAmountLocal;
                PlayerAmountOnline = tempAmountOnline;
                PlayerAmountOffline = tempAmountOffline;
            } else
            {
                _PlayerAmountLocal = 10000;
                _PlayerAmountOnline = 10000;
                _PlayerAmountOffline = 10000;
            }

            if (await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyOnline) && await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyOnlineHistory))
            {
                //ha már létezett a file akkor azt tölti be
                ObservableCollection<OnlineStockDataInvestment> tempOnlineList = await SaveLoadhelper.ReadFileAsync<ObservableCollection<OnlineStockDataInvestment>>(SaveLoadhelperKeyOnline);
                ObservableCollection<OnlineStockDataInvestment> tempOnlineHistoryList = await SaveLoadhelper.ReadFileAsync<ObservableCollection<OnlineStockDataInvestment>>(SaveLoadhelperKeyOnlineHistory);
                if (tempOnlineList != null) webStockDataInvestmentList = tempOnlineList;
                if (tempOnlineHistoryList != null) webStockDataInvestmentHistoryList = tempOnlineHistoryList;
            }

            if (await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyOffline) && await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyOfflineHistory))
            {
                //ha már létezett a file akkor azt tölti be
                ObservableCollection<OfflineStockDataInvestment> tempOfflineList = await SaveLoadhelper.ReadFileAsync<ObservableCollection<OfflineStockDataInvestment>>(SaveLoadhelperKeyOffline);
                ObservableCollection<OfflineStockDataInvestment> tempOfflineHistoryList = await SaveLoadhelper.ReadFileAsync<ObservableCollection<OfflineStockDataInvestment>>(SaveLoadhelperKeyOfflineHistory);
                if (tempOfflineList != null) offlineStockDataInvestmentList = tempOfflineList;
                if (tempOfflineHistoryList != null) offlineStockDataInvestmentHistoryList = tempOfflineHistoryList;
            }

            if (await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyAlphaVantageAPIKey))
            {
                AlphaVantageAPIKey = await SaveLoadhelper.ReadFileAsync<string>(SaveLoadhelperKeyAlphaVantageAPIKey);
            }

        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {
            try
            {
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyName, Name);
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyPlayerAmountLocal, PlayerAmountLocal);
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyPlayerAmountOnline, PlayerAmountOnline);
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyPlayerAmountOffline, PlayerAmountOffline);
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyOnline, webStockDataInvestmentList);
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyOnlineHistory, webStockDataInvestmentHistoryList);
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyOffline, offlineStockDataInvestmentList);
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyOfflineHistory, offlineStockDataInvestmentHistoryList);
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyAlphaVantageAPIKey, AlphaVantageAPIKey);
            }
            catch
            {
                //ha éppen foglalt a file, akkor excaptiont dob
            }
        }

        #region PropertyChangedEventHandler

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion
    }

}
