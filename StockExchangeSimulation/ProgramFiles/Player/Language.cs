﻿using Microsoft.Toolkit.Uwp.Helpers;
using System.Threading.Tasks;

namespace StockExchangeSimulation
{
    public class Language
    {
        public string language;
        LocalObjectStorageHelper SaveLoadhelper = new LocalObjectStorageHelper();
        readonly string SaveLoadhelperKeyLanguage = "Language";

        /// <summary>
        /// Constructor
        /// </summary>
        public Language()
        {
            LoadAsync();
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <returns></returns>
        public async Task LoadAsync()
        {

            if (await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyLanguage))
            {
                language = await SaveLoadhelper.ReadFileAsync<string>(SaveLoadhelperKeyLanguage);
                Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = language;
            }

        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {
            try
            {
                SaveLoadhelper.SaveFileAsync(SaveLoadhelperKeyLanguage, language);
            }
            catch
            {
                //ha éppen foglalt a file, akkor excaptiont dob
            }
        }

    }
}
