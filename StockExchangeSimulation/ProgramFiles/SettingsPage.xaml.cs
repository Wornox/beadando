﻿using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace StockExchangeSimulation
{
    /// <summary>
    /// SettingsPage
    /// </summary>
    public sealed partial class SettingsPage : Page
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SettingsPage()
        {
            this.InitializeComponent();
            if (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride == "en-EN") eng.IsChecked = true;
            else hun.IsChecked = true;
        }

        /// <summary>
        /// Handles the radibuttons change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            {
                string language = rb.Tag.ToString();
                Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = language;
                Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
                Windows.ApplicationModel.Resources.Core.ResourceContext.GetForViewIndependentUse().Reset();
                System.Threading.Thread.Sleep(500);
                if (language == "en-EN") {
                    hun.IsChecked = false;
                    eng.IsChecked = true;
                } else
                {
                    hun.IsChecked = true;
                    eng.IsChecked = false;
                }
                App.mainPageReference.homeTextBlock.Text = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNavHome");
                App.mainPageReference.playerStatsTextBlock.Text = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNavPlayerStat");
                App.mainPageReference.statisticsTextBlock.Text = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNavStatistics");
                App.mainPageReference.webstatisticsTextBlock.Text = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNavWebStatistics");
                App.mainPageReference.offlinestatisticsTextBlock.Text = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNavOfflineStatistics");

                App.Language.language = Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride;
                App.Language.SaveAsync();

            }
        }
    }
}
