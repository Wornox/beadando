﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.ApplicationModel.Resources;
using System.ComponentModel;
using Microsoft.Toolkit.Uwp.Helpers;
using System.Threading.Tasks;

namespace StockExchangeSimulation
{
    /// <summary>
    /// MainPage temporary variables
    /// </summary>
    public class MainPageProperties : INotifyPropertyChanged
    {
        private string _Language;
        public string Language
        {
            get { return _Language; }
            set
            {
                this._Language = value;
                RaisePropertyChanged("Language");
            }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                this._Name = value;
                RaisePropertyChanged("Name");
            }
        }

        #region PropertyChangedEventHandler

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion 
    }

    /// <summary>
    /// MainPage main class
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public MainPage()
        {
            App.mainPageReference = this;

            this.InitializeComponent();

            EnableMenuItems(false);

            Task skip = SkipMainPageAsync();

            LanguageCheck();

        }

        /// <summary>
        /// Checks to skip the MainPage default contetnPage and replace it with HomePage
        /// </summary>
        /// <returns></returns>
        private async Task SkipMainPageAsync()
        {
            LocalObjectStorageHelper SaveLoadhelper = new LocalObjectStorageHelper();
            string SaveLoadhelperKeyName = "PlayerName";

            if (await SaveLoadhelper.FileExistsAsync(SaveLoadhelperKeyName))
            {
                App.Player.LoadAsync();
                EnableMenuItems(true);
                contentFrame.Navigate(typeof(HomePage));
            }
        }
        
        /// <summary>
        /// Handles the menu bar item click actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void NavigationViewControl_ItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            if (args.IsSettingsInvoked)
            {
                if (HomeNavigationViewItem.IsEnabled)
                {
                    contentFrame.Navigate(typeof(SettingsPage));
                }
            }
            else
            {
                TextBlock ItemContent = args.InvokedItem as TextBlock;
                if (ItemContent != null)
                {
                    switch (ItemContent.Tag)
                    {
                        case "Nav_Home":
                            contentFrame.Navigate(typeof(HomePage));
                            break;

                        case "Nav_Player":
                            contentFrame.Navigate(typeof(PlayerPage));
                            break;

                        case "Nav_Statistics":
                            contentFrame.Navigate(typeof(LocalStockDataPage));
                            break;

                        case "Nav_WebStatistics":
                            contentFrame.Navigate(typeof(OnlineStockDataPage));
                            break;

                        case "Nav_OfflineStatistics":
                            contentFrame.Navigate(typeof(OfflineStockDataPage));
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// SaveAsync button click event, saves the setting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            EnableMenuItems(true);

            App.Player.Name = nameTextBox.Text;

            if (String.IsNullOrEmpty(APITextBox.Text)) App.Player.AlphaVantageAPIKey = "default";
            else App.Player.AlphaVantageAPIKey = APITextBox.Text;

            App.Language.language = Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride;
            App.Language.SaveAsync();

            App.Player.SaveAsync();

            contentFrame.Navigate(typeof(HomePage));

        }

        /// <summary>
        /// Handles the language radiobutton changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            {
                string language = rb.Tag.ToString();
                Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = language;
                Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
                Windows.ApplicationModel.Resources.Core.ResourceContext.GetForViewIndependentUse().Reset();
                System.Threading.Thread.Sleep(500);
                if (language == "en-EN")
                {
                    hunMain.IsChecked = false;
                    engMain.IsChecked = true;
                }
                else
                {
                    hunMain.IsChecked = true;
                    engMain.IsChecked = false;
                    
                }

                ReloadStrings();
            }
        }

        /// <summary>
        /// Enables or disables the menu items
        /// </summary>
        /// <param name="enable">Set to true to enable, set to false to disable</param>
        private void EnableMenuItems(bool enable)
        {
            HomeNavigationViewItem.IsEnabled = enable;
            PlayerNavigationViewItem.IsEnabled = enable;
            StatisticsNavigationViewItem.IsEnabled = enable;
            WebStatisticsNavigationViewItem.IsEnabled = enable;
            OfflineStatisticsNavigationViewItem.IsEnabled = enable;
        }

        /// <summary>
        /// Set the radibuttons to represent the selected language 
        /// </summary>
        private void LanguageCheck()
        {
            if (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride == "en-EN") engMain.IsChecked = true;
            else hunMain.IsChecked = true;
            Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
            Windows.ApplicationModel.Resources.Core.ResourceContext.GetForViewIndependentUse().Reset();
        }

        /// <summary>
        /// Reload the viewed string to the current language
        /// </summary>
        private void ReloadStrings()
        {
            languageTextBlock.Text = ResourceLoader.GetForCurrentView().GetString("MainPageLanguage");
            homeTextBlock.Text = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNavHome");
            playerStatsTextBlock.Text = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNavPlayerStat");
            statisticsTextBlock.Text = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNavStatistics");
            webstatisticsTextBlock.Text = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNavWebStatistics");
            offlinestatisticsTextBlock.Text = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNavOfflineStatistics");

            nameTextBox.Header = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNameHeader");
            nameTextBox.PlaceholderText = ResourceLoader.GetForViewIndependentUse().GetString("MainPageNamePlaceholder");

            APITextBox.Header = ResourceLoader.GetForViewIndependentUse().GetString("MainPageAPIHeader");
            APITextBox.PlaceholderText = ResourceLoader.GetForViewIndependentUse().GetString("MainPageAPIPlaceholder");

            saveButton.Content = ResourceLoader.GetForViewIndependentUse().GetString("MainPageSaveButton");

        }

    }

}