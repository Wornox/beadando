﻿using System;
using System.Collections.ObjectModel;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace StockExchangeSimulation
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        #region AppStaticVariables
        static public MainPage mainPageReference;

        static public Language Language;

        static public Player Player { get; set; }

        //Local
        static public LocalStockDataProgressBar Refreshprogressbar { get; set; }
        static public LocalStockDataHistory CompanyStockDataHistory;
        static public LocalStockDataSimulation CompanyStockDataSimulation;
        static public LocalStockDataSaveLoad CompanyStockDataSaveLoad;
        static public ObservableCollection<LocalStockData> CompanyStockDataList { get; set; }
        static public LocalStockDataGraphViewModel GraphDataList { get; set; }

        //Online
        static public OnlineStockData WebStockDatas;
        static public OnlineStockDataDisplayCount WebStocksDisplayCount;
        static public OnlineStockDataProgressBar OnlineStockDataProgressBar { get; set; }

        //Offline
        static public OfflineStockDataProgressBar OfflineStockDataProgressBar { get; set; }
        static public OfflineStockData OfflineStockDatas;
        static public OfflineStockDataHistoryDisplay OfflineStockDataHistoryDisplay;
        static public OfflineStockDataTimerComboBox OfflineStockDataTimerComboBox;
        #endregion

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            StartUpAsync();
            this.InitializeComponent();
            this.Suspending += OnSuspending;
        }

        /// <summary>
        /// Creating new statics
        /// </summary>
        private async void StartUpAsync()
        {
            Language = new Language();

            Player = new Player();

            //Local
            CompanyStockDataSaveLoad = new LocalStockDataSaveLoad();
            await CompanyStockDataSaveLoad.LoadAsync();

            CompanyStockDataHistory = new LocalStockDataHistory();
            await CompanyStockDataHistory.LoadAsync();

            CompanyStockDataSimulation = new LocalStockDataSimulation();
            Refreshprogressbar = new LocalStockDataProgressBar(5);

            GraphDataList = new LocalStockDataGraphViewModel();

            //Online
            WebStockDatas = new OnlineStockData();
            WebStocksDisplayCount = new OnlineStockDataDisplayCount();
            OnlineStockDataProgressBar = new OnlineStockDataProgressBar();

            //Offline
            OfflineStockDataProgressBar = new OfflineStockDataProgressBar(5);
            OfflineStockDatas = new OfflineStockData();
            OfflineStockDataHistoryDisplay = new OfflineStockDataHistoryDisplay();
            OfflineStockDataTimerComboBox = new OfflineStockDataTimerComboBox();

        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: LoadAsync state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (e.PrelaunchActivated == false)
            {
                if (rootFrame.Content == null)
                {
                    // When the navigation stack isn't restored navigate to the first page,
                    // configuring the new page by passing required information as a navigation
                    // parameter
                    rootFrame.Navigate(typeof(MainPage), e.Arguments);
                }
                // Ensure the current window is active
                Window.Current.Activate();
            }
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: SaveAsync application state and stop any background activity
            deferral.Complete();
        }
    }
    
}
